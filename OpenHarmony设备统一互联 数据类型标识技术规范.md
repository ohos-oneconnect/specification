# 1. 范围
本文件规定了基于OpenHarmony的设备在进行互联互通业务时相关的数据类型标识，使得不同厂商的OpenHarmony设备之间的数据类型标识统一，为高效的数据互通提供基础。

本文适用于基于OpenHarmony的设备跨应用、跨设备数据交互场景，设备包括但不限于手机、平板、PC等。

# 2. 规范性引用文件
下列文件中的内容通过文中的规范性引用而构成本文件必不可少的条款。其中，注日期的引用文件，仅该日期对应的版本适用于本文件；不注日期的引用文件，其最新版本（包括所有的修改单）适用于本文件。   
RFC 2048 多用途互联网邮件扩展第四部分：注册程序(Multipurpose Internet Mail Extensions(MIME) Part Four:Registration Procedures)

RFC 2046 多用途互联网邮件扩展第二部分：媒体类型(Multipurpose Internet Mail Extensions(MIME) Part Two:Media Types)

RFC 2234 扩展的巴科斯范式(Augmented BNF for Syntax Specifications: ABNF)

# 3. 术语和定义
下列术语和定义适用于本文件。

## 3.1
**生态终端 OneConnect Device**   
在基于OpenHarmony底座的设备生态系统中，连接到互联互通网络中，可以执行控制类终端的交互指令，并满足人们对环境的智能化应用需求的电子化、信息化产品。

## 3.2
**通用互联APP OneConnect APP**   
在基于OpenHarmony底座的设备生态系统中，运行在智能设备中，用于生态设备管理应用程序。

# 4. 符号和缩略语
下列缩略语适用于本文件。

| 缩略语 | 英文全名 | 中文解释 |
| --- | --- | --- |
| UTD | Uniform Type Descriptor | 标准化数据类型 |
| MIME | Multipurpose Internet Mail Extensions | 多用途互联网邮件扩展类型 |
| URL| Uniform Resource Locator | 统一资源定位符 |
| JPEG | Joint Photographic Experts Group | 联合图像专家组 |


# 5. 标准化数据类型
当前生态终端之间传输数据存在类型模糊的问题，即针对同一种数据类型，存在不同的类型描述方式：MIME Type、文件扩展名等，例如描述jpg/jpeg类型图片时，可以使用image/jpeg、.jpg、.jpeg或image/picture等方式进行描述。当相关类型的数据进行跨应用、跨设备传输时，目标端应用/设备需要进行多方面的适配，才能够对数据内容进行相关处理，且存在无法识别的情况。

为了解决生态终端之间传输数据的类型模糊问题，本文提出了标准化数据类型（Uniform Type Descriptor，简称UTD）的概念。

## 5.1 标准化数据类型的定义

标准化数据类型包含了标准化数据类型的标识ID、归属类型关系、简要描述等信息，具体可见[TypeDescriptor属性](../../../../openharmony/docs/blob/master/zh-cn/application-dev/reference/apis-arkdata/js-apis-data-uniformTypeDescriptor.md#属性)，每个类型定义具体包含以下内容：

+ **TypeId：** 定义标准化数据类型的ID，该ID具有唯一性。
+ **BelongingToTypes：** 定义标准化数据类型的归属关系，即该标准化数据类型归属于哪个更高层级的类型，允许存在一个标准化数据类型归属于多个类型的情况。
+ **Description：** 标准化数据类型的简要说明，描述基本信息和使用场景。
+ **ReferenceURL：** 标准化数据类型的参考链接URL，用于描述类型的详细信息。
+ **IconFile：** 标准化数据类型的默认图标文件路径，可能为空字符串（即没有默认图标），应用可以自行决定是否使用该默认图标。
+ **FilenameExtensions：**  标准化数据类型所关联的文件名后缀列表。
+ **MIMETypes：** 标准化数据类型所关联的多用途互联网邮件扩展类型列表。

定义举例：

​    "TypeId" : "com.example.drag2.document",

​    "BelongingToTypes" : [

​         "general.object"

​    ],

​    "FilenameExtensions" : [

​        ".mydocument",

​        ".mydoc"

​    ],

​    "MIMETypes" : [

​        "application/my-document",

​        "application/my-doc"

​    ],

​    "Description" : "My document",

​    "ReferenceURL" : "https://www.mycompany.com/my-document",

​    "IconFile" : "resources/my-document.png",

## 5.2 标准化数据类型的分类   
UTD中定义的标准化数据类型分为物理和逻辑两类，可以从两个维度描述同一个数据类型：

- **按物理分类**的根节点为general.entity，用于描述类型的物理属性，比如文件、目录等，具体可见图1。

- **按逻辑分类**的根节点为general.object，用于描述类型的功能性特征，如图片、网页等，具体可见图2。

如描述图片时，可以是一个图片对象，同时也可以是一个文件，但并非所有的格式都具有两个维度，如日程类的数据，更多注重的是功能性描述。

**图1** 物理标准化数据类型示意图

![utd_preset_type](image/utd_preset_type.png)

**图2** 逻辑标准化数据类型示意图

![utd_type](image/utd_type.png)

## 5.3 标准化数据类型的扩展
- **基础数据类型** 

  基础数据类型表示通用数据类型，进行跨应用、跨生态终端、跨平台交互时，能够被绝大多数应用、生态终端以及平台识别，标识ID为"general.xxx"，当前已经预先定义了部分基础数据类型，可参考[UTD预置列表-基础类型](../../../../openharmony/docs/blob/master/zh-cn/application-dev/database/uniform-data-type-list.md#基础类型)。

- **系统关联数据类型** 

  系统关联数据类型是与具体的平台/操作系统有较为深入关联的数据类型，支持平台/操作系统内跨应用的交互，标识ID为"os-name.xxx"，当前OpenHarmony操作系统中已预先定义了部分系统关联数据类型，可参考[UTD预置列表-系统关联类型](../../../../openharmony/docs/blob/master/zh-cn/application-dev/database/uniform-data-type-list.md#系统关联类型)。

- **应用自定义数据类型**

  预置的数据类型无法穷举所有数据类型，在业务跨应用、跨生态终端交互过程中，会涉及到一些应用独有的数据类型，因此支持应用声明自定义数据类型。

  应用自定义类型的标识ID为com.company/group.xxx或者org.company/group.xxx。当前已预定义了部分自定义数据类型，可以参考[UTD预置列表-应用定义类型](../../../../openharmony/docs/blob/master/zh-cn/application-dev/database/uniform-data-type-list.md#应用定义类型)。

- **冲突处理**

  由于基础、系统关联（这两种以下简称预置）、应用自定义（以下简称自定义）三类标准化数据类型的类型标识符必须具备唯一性，在增加新的自定义类型标识符时，不可避免会出现冲突的情况。当前提供基础的自动冲突解决原则，具体定义规则如下：

  1、当新的自定义与预置的类型标识符发生冲突时，采用预置中的类型标识符定义，自定义的类型标识符不生效；

  2、当新的自定义的类型标识符与已有的类型标识符后缀名冲突时，采用"先到先得"的策略，新的自定义的类型标识符不生效；

  3、当新的自定义的类型中MIME Type与已有的类型中MIME Type冲突时，采用"先到先得"的策略，新的自定义的类型标识符不生效；

  4、以上1-3条，适用于生态终端内部的冲突，对于跨生态终端的场景，新的自定义类型在注册的时会绑定分布式时间戳，出现冲突时，基于分布式时间戳进行冲突处理，处理原则与生态终端内部的原则一致。

# 6. 标准化数据类型的工作原理

基于标准类型的层级结构，业务声明自己支持的数据类型标识符时，需要声明该类型标识符的层级逻辑，例如业务自定义图片类型UTD标识符“com.company.x-image”，并归属到general.image类中。UTD会检验自定义类型标识符，确保归属关系中不出现环状结构。

应用安装时，UTD会读取应用中自定义的数据类型进行安装，校验自定义类型数据符合约束条件后，应用自定义数据类型将被安装到设备中。应用启动后能正常读取到应用自定义的数据类型。如果引用其他应用定义的自定义数据类型，需要在应用开发时一并写入自定义数据类型配置文件中。

## 6.1 约束限制

针对自定义的类型描述各字段，有以下相关要求和限制：

+ **TypeId：** 定义标准化数据类型的ID，该ID具有唯一性，由应用bundleName + 具体类型名组成，不可缺省，允许包含数字、大小写字母、-和.。

+ **BelongingToTypes：** 定义标准化数据类型的归属关系，即该标准化数据类型归属于哪个更高层级的类型，所属类型可以为多个，但是必须为已存在的数据
  类型（标准化数据类型预置类型或其他新增自定义数据类型），不能为应用自定义类型本身，不能为空，且与现有标准化数据类型、其他新增自定义数据类型不能形成环行依赖结构。

+ **FilenameExtensions：** 应用自定义标准化数据类型所关联的文件后缀。可以缺省；可以为多个，每个后缀为以.开头且长度不超过127的字符串。

+ **MIMETypes：** 应用自定义标准化数据类型所关联的web消息数据类型。可以缺省；可以为多个，每个类型为长度不超过127的字符串。

+ **Description：** 应用自定义标准化数据类型的简要说明。可以缺省；填写时，长度为不超过255的字符串。

+ **ReferenceURL：** 应用自定义标准化数据类型的参考链接URL，用于描述类型的详细信息。可以缺省；填写时，长度为不超过255的字符串。


## 6.2 开发步骤

下面以新增媒体类文件类型场景为例，说明如何自定义UTD标准化数据类型。

1. 当前应用在entry\src\main\resources\rawfile\arkdata\utd\目录下新增utd.json5文件。

2. 在当前应用的utd.json5配置文件内新增所需的自定义数据类型。

   ```json
   {
        "UniformDataTypeDeclarations": [
            {
                "TypeId": "com.example.myFirstHap.image",
                "BelongingToTypes": ["general.image"],
                "FilenameExtensions": [".myImage", ".khImage"],
                "MIMETypes": ["application/myImage", "application/khImage"],
                "Description": "My Image.",
                "ReferenceURL": ""
            },
            {
                "TypeId": "com.example.myFirstHap.audio",
                "BelongingToTypes": ["general.audio"],
                "FilenameExtensions": [".myAudio", ".khAudio"],
                "MIMETypes": ["application/myAudio", "application/khAudio"],
                "Description": "My audio.",
                "ReferenceURL": ""
            },
            {
                "TypeId": "com.example.myFirstHap.video",
                "BelongingToTypes": ["general.video"],
                "FilenameExtensions": [".myVideo", ".khVideo"],
                "MIMETypes": ["application/myVideo", "application/khVideo"],
                "Description": "My video.",
                "ReferenceURL": ""
            }
        ]
   }
   ```

3. 如果其他应用要直接使用当前应用内的自定义数据类型，需要在其应用的entry\src\main\resources\rawfile\arkdata\utd\目录下新增utd.json5文件。

   然后在utd.json5配置文件中进行以下声明：

   ```json
   {
       "ReferenceUniformDataTypeDeclarations": [
            {
                "TypeId": "com.example.myFirstHap.image",
                "BelongingToTypes": ["general.image"],
                "FilenameExtensions": [".myImage", ".khImage"],
                "MIMETypes": ["application/myImage", "application/khImage"],
                "Description": "My Image.",
                "ReferenceURL": ""
            }
       ]
   }
   ```

4. 其他应用也可以在DevEco Studio中创建utd.json5模板，在模板中引用当前应用内的自定义数据类型之后，基于已引用的自定义数据类型进行自定义。同时，DevEco Studio还会对配置文件中的字段进行格式校验，utd.json5配置文件示例如下：

   ```json
   {
       "UniformDataTypeDeclarations": [
           {
               "TypeId": "com.example.mySecondHap.image",
               "BelongingToTypes": ["com.example.myFirstHap.image"],
               "FilenameExtensions": [".myImageEx", ".khImageEx"],
               "MIMETypes": ["application/my-ImageEx", "application/khImageEx"],
               "Description": "My Image extension.",
               "ReferenceURL": ""
           }
       ]
   }
   ```

## 6.3 接口说明

以下是UTD常用接口说明，对于预置数据类型和应用自定义数据类型同样适用，更多接口和详细说明，请见[ @ohos.data.uniformTypeDescriptor (标准化数据定义与描述)](../../../../openharmony/docs/blob/master/zh-cn/application-dev/reference/apis-arkdata/js-apis-data-uniformTypeDescriptor.md)。

| 接口名称                                                     | 描述                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| UniformDataType                                              | 标准化数据类型的枚举定义。此处不再展开列举各枚举。           |
| belongsTo(type: string): boolean                             | 判断当前标准化数据类型是否归属于指定的标准化数据类型。       |
| isLowerLevelType(type: string): boolean                      | 判断当前标准化数据类型是否是指定标准化数据类型的低层级类型。 |
| isHigherLevelType(type: string): boolean                     | 判断当前标准化数据类型是否是指定标准化数据类型的高层级类型。 |
| getUniformDataTypeByFilenameExtension(filenameExtension: string, belongsTo?: string): string | 根据给定的文件后缀名和所归属的标准化数据类型查询标准化数据类型ID，若有多个符合条件的标准化数据类型ID，则返回第一个。 |
| getUniformDataTypeByMIMEType(mimeType: string, belongsTo?: string): string | 根据给定的MIME类型和所归属的标准化数据类型查询标准化数据类型ID，若有多个符合条件的标准化数据类型ID，则返回第一个。 |
| getUniformDataTypesByFilenameExtension(filenameExtension: string, belongsTo?: string): Array\<string> | 根据给定的文件后缀名和所归属的标准化数据类型查询标准化数据类型ID列表。 |
| getUniformDataTypesByMIMEType(mimeType: string, belongsTo?: string): Array\<string> | 根据给定的MIME类型和所归属的标准化数据类型查询标准化数据类型ID列表。 |
