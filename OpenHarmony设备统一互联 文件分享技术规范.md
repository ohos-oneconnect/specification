# 1 范围
本文规定了鸿蒙生态设备之间文件互传发现、接入认证和文件传输规范，满足不同厂商的鸿蒙生态设备之间文件能够相互分享。本文适用于鸿蒙生态设备文件分享特性。

# 2 规范性引用文件
下列文件中的内容通过文中的规范性引用而构成本文件必不可少的条款。其中，注日期的引用文件，仅该日期对应的版本适用于本文件；不注日期的引用文件，其最新版本（包括所有的修改单）适用于本文件。

Bluetooth Core Specification, Version 5.2, Bluetooth SIG

Wi-Fi_Peer-to-Peer_Services_Technical_Specification, Version 1.2, Wi-Fi Alliance

# 3 术语和定义
下列术语和定义适用于本文件。

# 4 符号和缩略语
# 5 用词约定
规则：必须遵守的约定

建议：需要加以考虑的约定

说明：对此规则或建议进行相应的解释

参考：详细说明附带链接

# 6 概述
## 6.1特性概述
OpenHarmony L2设备（Source端）文件管理器和图库可以选择文件、图片分享给OpenHarmony L2设备。OpenHarmony L2设备接收到文件支持保存到本地。



## 6.2 硬件版本
海思V352大屏

海思V900大屏

海思3519DV500

## 6.2 软件版本
OpenHarmony系统版本基线：基于 OpenHarmony-v5.0.0-Release。

图库应用版本：基于OpenHarmony-v5.0.0-Release。

文件管理器应用版本：基于OpenHarmony-v5.0.0-Release。

# 7 用户历程图
![用户历程图](image/文件分享/用户历程图.png)

# 8 设备发现
## 8.1 设备交互流程图
![设备交互流程图](image/文件分享/设备交互流程图.png)

## 8.2  设备发现流程
![设备发现流程图](image/文件分享/设备发现流程图.png)

## 8.3  发现报文
OpenHarmony生态设备统一分享ADV报文

| 字段 | 长度（byte） | 数值 | 描述 | 是否必选 |
| :--- | :---: | :---: | --- | :---: |
| Length | 1 | 0x02 | Length后面报文长度 | 是 |
| Type | 1 | 0x16 | ADType，后续ADData使用UUID&自定义扩展 | 是 |
| UUID | 2 | 0xFE35 | 华为分享UUID | 是 |
| CmdId | 1 | 0x2E | 命令字 | 是 |
| TypeList | 1 | XX | 数据类型集合，bit0~8对应8种数据类型（bit0:BTName，bit1:DeviceType bit2:UserToken) | 是 |
| DevName | 18 | XX | 设备名 | 是 |
| DeviceType | 2 | XX | 设备类型：用于标识设备形态，每bit标识一个type（剩余待扩展）：<br/>TYPE_1_0_PHONE = 1<br/>TYPE_2_0_PHONE = 2<br/>TYPE_PC = 3 <br/>TYPE_TV = 4<br/>TYPE_PAD = 5<br/>TYPE_BLE_CONNECT_PC = 6<br/>TYPE_WATCH=7<br/>TYPE_CAR=8 | 是 |
| UserToken | 2 | XX | 用户应用token，恢复出厂后重新生成 | 是 |
| Reserved | 3 | XX | 保留字段 | 否 |

|  |  |  |  |  |  |
| --- | :---: | :---: | --- | --- | --- |
|  | Extended Broadcasting（N bytes） | | | | |
| AdvA(广播地址) | AdvData(广播数据) | | | | |
|  | AD Sturcture1 | AD Sturcture2 | AD Sturcture3 | AD AD Sturcture4 |  |
| AD Sturcture1： |  |  |  |  |  |
| 字段 | 长度（byte） | 数值(示例) | 描述 | | 是否必选 |
| Length | 1 | 0x02 | AD Type和AD Data字段的总长度 | | 是 |
| AD Type | 1 | 0x01 | 表示“Flags”类型的数据。Flags类型的数据用于提供关于广播设备的一些基本信息，如是否支持LE General Discoverable Mode（LE通用可发现模式）等。 | | 是 |
| AD Data | 1 | 0x02 | 表示蓝牙设备物理连接能力，支持ble通用可发现模式，且支持br/edr | | 是 |
| AD Sturcture2： |  |  |  |  |  |
| Length | 1 | 0x1B | AD Type和AD Data字段的总长度 | | 是 |
| AD Type | 1 | 0x16 | 数据结构包含的是Service Data,前两个字节表示一个16位的UUID,用于向扫描设备提供有关广播设备所支持的服务的信息 | | 是 |
| AD Data | 2 | 0xEEFD | UUID（通用唯一标识符，固定） | | 是 |
| | 1 | 0x04 | 蓝牙协议版本 | | 是 |
| | 1 | 0x05 | 软总线业务 | | 是 |
| | 1 | 0x10 | bit0 | 当advId == CON_ADV_ID且isWakeRemote时该位置一； | 否 |
| | | | bit4 |  | 是 |
| | | | bit7 | 当advId == CON_ADV_ID时该位置一； | 否 |
| | 2 | 0x5FEC | 当advId == CON_ADV_ID时从DeviceInfo中获取accountHash；<br/>当advId != CON_ADV_ID时用LnnGetLocalByteInfo从BYTE_KEY_ACCOUNT_HASH中获取accountHash； | | 是 |
| | 1 | 0x10 | capabilityBitmap | | 是 |
| | 1 | 0x00 |  | | 是 |
| | 1 | 0x18 | 8 | DATA_LENGTH | 是 |
| | | | 0x1 | DATA_TYPE | 是 |
| | 8 | 0x44C70F51CE5E60AA | 设备ID的HASH值 | | 是 |
| | 1 | 0x21 | 1 | DATA_LENGTH | 是 |
| | | | 0x2 | DATA_TYPE | 是 |
| | 1 | 0x11 | 设备类型 | | 是 |
| | 6 | 0x3B5A61694F48 | 蓝牙mac地址 | | 是 |
| AD Sturcture3： |  |  |  |  |  |
| Length | 1 | 0x09 | AD Type和AD Data字段的总长度 | |  |
| AD Type | 1 | 0xFF | 厂商自定义数据 | |  |
| AD Data | 2 | 0x7D02 | 厂商id | |  |
| | 6 | 0x4F532D463800 | beacon | |  |
| AD Sturcture4： |  |  |  |  |  |
| Length | 1 | 0x10 | AD Type和AD Data字段的总长度 | |  |
| AD Type | 1 | 0x09 | 设备简称 | |  |
| AD Data | 15 | 0x426C7565746F6F7468446576696365 | 名称 | |  |

# 9 设备连接
## 9.1 设备连接认证流程	
![设备连接认证流程图](image/文件分享/设备连接认证流程图.png)

## 9.2 BLE 连接
## 9.3 设备认证
### 9.3.1 无账号PIN认证
![无账号PIN认证流程图](image/文件分享/无账号PIN认证流程图.png)

## 9.4 WiFi 连接
### 9.4.1 P2P连接
通过软总线在OpenSession时，选择.linkType[0] = LINK_TYPE_WIFI_P2P，由软总线自动建立p2p连接。

# 10 文件传输
## 10.1 传输协议
## 10.2 传输流程
![传输流程图](image/文件分享/传输流程图.png)

## 10.3 图片传输
图片传输的流程如下：

![图片传输流程图](image/文件分享/图片传输流程图.png)

建立图片传输会话时，会话类型为图片传输，并且在会话信息里需要带上图片和视频缩略图的base64编码。发送和接收端都在图片传输开始时显示图片和视频的缩略图。

### 10.3.1 单图片/视频传输
单图片传输时，会话和传输列表只包含一个文件。传输进度按文件发送的字节大小百分比计算，取整数。

### 10.3.2 多图片/视频传输
多图片传输时，会话和传输列表只包含多个文件，可以使图片和适配混合一起发送。传输进度按文件发送的字节大小百分比计算，取整数。

## 10.4.1 文件传输
文件传输的流程如下：

![文件传输流程图](image/文件分享/文件传输流程图.png)

建立文件传输会话时，会话类型为文件传输，会话信息里缩略图字段为空。发送和接收端都在图片传输开始时显示文件列表。

### 10.4.1 单文件传输
单文件传输时，会话和传输列表只包含一个文件。传输进度按文件发送的字节大小百分比计算，取整数。

### 10.4.2 多文件传输
多文件传输时，会话和传输列表只包含多个文件。传输进度按文件发送的字节大小百分比计算，取整数。

## 10.5 文件目录传输
选择文件目录传输时，单次传输只能选择1个文件夹。取文件夹里的文件列表进行传输，当文件夹为空时，不允许传输，并且提示用户。传输进度按文件发送的字节大小百分比计算，取整数。

# 11 接口定义
此章节定义和描述文件分享SDK的NAPI相关接口，提供给应用层APP调用。

## 11.1. 创建文件分享管理实例
createShareManager(bundleName: string): ShareManager

创建文件分享管理实例。

系统能力：

SystemCapability.Communication.Share

需要权限：

参数：

无

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |


## 11.2. 释放文件分享管理实例
releaseShareManager(shareManager: ShareManager): void

释放文件分享管理实例，释放系统资源。

系统能力：

SystemCapability.Communication.Share

需要权限：

参数：

无

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |


## 11.3. 开启文件分享
ShareManager.enableShare(): void

Sink端开启文件分享能力，启动设备文件分享BLE广播，使设备可以被文件分享发现。

系统能力：

SystemCapability.Communication.Share

需要权限：

参数：

无

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.4. 关闭文件分享
ShareManager.disableShare(): void

Sink端关闭文件分享能力，停止设备文件分享BLE广播，设备无法被文件分享发现。

系统能力：

SystemCapability.Communication.Share

需要权限：

参数：

无

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed.<br/> |

## 11.5. 获取文件分享状态
ShareManager.getShareStatus(): boolean

Sink端获取文件分享状态。

系统能力：

SystemCapability.Communication.Share

需要权限：

返回值：

| 类型 | 说明 |
| :--- | :--- |
| boolean | 文件共享状态。true 表示文件共享开启，false表示文件共享关闭。 |


错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.6. 开始扫描设备
ShareManager.startScan(): void

Source端开始扫描文件分享设备广播。

系统能力：

SystemCapability.Communication.Share

需要权限：

参数：

无

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.7. 停止扫描设备
ShareManager.stopScan(): void

Source端停止扫描文件分享设备广播。

系统能力：

SystemCapability.Communication.Share

需要权限：

参数：

无

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.8. 连接设备
ShareManager.connect(deviceId: string, bindParam: { [key: string]: Object },  
 callback: AsyncCallback<{ deviceId: string }>): void

Source端连接对端设备。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| deviceId | string | 是 | 设备ID。 |
| bindParam | Object | 是 | 连接设备相关参数。 |
| callback | AsyncCallback | 是 | 连接设备状态回调。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.9. 关闭连接
ShareManager.disconnect(deviceId: string): void

Source端取消设备连接。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| deviceId | string | 是 | 设备ID。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.10. 接受文件分享连接
ShareManager.confirmConnect(): void

Sink端确认接受文件分享连接。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.11. 拒绝文件分享连接
ShareManager.refuseConnect(): void

Sink拒绝文件分享，终止文件分享流程。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.12. 发送文件
ShareManager.sendFile(deviceId: string, type: ShareType, files: Array<ShareFileInfo>): void

Source端发送文件内容。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| deviceId | string | 是 | 设备ID。 |
| type | ShareType | 是 | 分享类型 |
| files | Array<ShareFileInfo> | 是 | 文件信息数组 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.12. 取消发送文件
ShareManager.cancelSendFile(deviceId: string): void

Source端取消发送文件。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| deviceId | string | 是 | 设备ID。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.13. 终止接收文件
ShareManager.abortReceiveFile(deviceId: string): void

Sink端终止接收文件内容。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| deviceId | string | 是 | 设备ID。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.14. 注册连接改变事件
ShareManager.on(type: 'connectionChange', callback: Callback<{

action: ShareDeviceStateChange;

device: ShareDeviceInfo;

}>): void

注册连接状态改变事件。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| type | string | 是 | 固定填"connectionChange"字符串。 |
| callback | Callback | 是 | 共享设备列表改变回调函数。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.15. 取消注册连接状态改变事件
ShareManager.off(type: 'connectionChange'): void

取消注册连接状态改变事件。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| type | string | 是 | 固定填"connectionChange"字符串。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.16. 注册发现设备成功事件
ShareManager.on(type: 'discoverSuccess', callback: Callback<{

device: ShareDeviceInfo;

}>): void

注册发现设备成功事件。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| type | string | 是 | 固定填"discoverSuccess"字符串。 |
| callback | Callback | 是 | 发现设备成功回调函数。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.17. 取消注册发现设备成功事件
ShareManager.off(type: 'discoverSuccess'): void

取消注册发现设备成功事件。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| type | string | 是 | 固定填"discoverSuccess"字符串。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.18. 注册发现设备失败事件
ShareManager.on(type: 'discoverFailure', callback: Callback<{

reason: number;

}>): void

注册发现设备失败事件。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| type | string | 是 | 固定填"discoverFailure"字符串。 |
| callback | Callback | 是 | 发现设备失败回调函数。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.19. 取消注册发现设备失败事件
ShareManager.off(type: 'discoverFailure'): void

取消注册发现设备失败事件。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| type | string | 是 | 固定填"discoverFailure"字符串。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.20. 注册文件分享开关状态改变事件
ShareManager.on(type: 'shareStatusChange', callback: Callback<boolean>): void

注册文件分享开关状态改变事件。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| type | string | 是 | 固定填"shareStatusChange"字符串。 |
| callback | Callback<boolean> | 是 | 分享状态改变回调函数。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |

## 11.21. 取消注册文件分享状态改变事件
ShareManager.off(type: 'shareStatusChange'): void

取消注册文件分享状态改变事件。

系统能力： 

SystemCapability.Communication.Share

需要权限：

参数：

| 参数名 | 类型 | 必填 | 说明 |
| :--- | :--- | :--- | :--- |
| type | string | 是 | 固定填"shareStatusChange"字符串。 |

错误码：

| 错误码ID | 错误信息 |
| :--- | :--- |
| 201 | Permission denied. |
| 202 | System API is not allowed called by Non-system application. |
| 801 | Capability not supported. |
| 2501000 | Operation failed. |
