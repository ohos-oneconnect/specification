# 1. 范围
本文件规定了鸿蒙生态设备互联互通互操作业务相关的测试特性和用例，主要包括Wi-Fi、BLE、以太网等类型设备的发现、配网、连接、查询和控制等功能性测试。   
本文件适用于指导基于OpenHarmony操作系统的生态设备，验证其互联互通互操作业务能力，确保OpenHarmony系统上互联互通组件运行稳定、功能正常，为用户提供便捷、流畅、安全可靠的一致生态交互体验。
# 2. 规范性引用文件
下列文件中的内容通过文中的规范性引用而构成本文件必不可少的条款。其中，注日期的引用文件，仅该日期对应的版本适用于本文件；不注日期的引用文件，其最新版本（包括所有的修改单）适用于本文件。   
T/GIIC XXXXX—XXXX 鸿蒙生态设备互联互通互操作接入与控制接口规范   
T/GIIC XXXXX—XXXX 鸿蒙生态设备互联互通互操作 物模型规范   
# 3. 术语和定义
下列术语和定义适用于本文件。
## 3.1
**鸿蒙生态云平台 HarmonyConnect Cloud**   
通过网络统一组织和灵活调用各种鸿蒙生态信息资源，实现鸿蒙生态信息大规模计算的处理方式。其利用分布式计算和虚拟资源管理等技术，通过网络将分散的ICT资源（包括计算与存储、应用运行平台、软件等）集中起来形成共享的鸿蒙生态资源池，并以动态按需和可度量的方式向用户提供服务。以下简称云平台或者云。
## 3.2
**鸿蒙生态终端 HarmonyConnect Device**   
在鸿蒙生态系统中，连接到互联互通网络中，可以执行控制类终端的交互指令，并满足人们对环境的智能化应用需求的电子化、信息化产品。以下简称生态终端。
## 3.3
**鸿蒙生态配置器 HarmonyConnect Configurator**   
配置应用终端并将其接入家庭局域网络的逻辑实体，可能集成在智能手机、智能电视、智能音箱、智能路由器等设备的应用中。以下简称配置器。
## 3.4
**鸿蒙生态通用互联APP HarmonyConnect APP**   
在鸿蒙生态系统中，运行在智能设备中，用于生态设备管理应用程序。以下简称通用互联APP。
## 3.5
**鸿蒙生态桥接设备 HarmonyConnect Bridge Device**   
在鸿蒙生态系统中，开源鸿蒙生态桥接设备用于将非IP设备加入互联互通网络中。以下简称桥设备或者桥。
## 3.6
**鸿蒙生态点到点本地控制 HarmonyConnect Point to Point Operation**   
在鸿蒙生态系统中，为了促进设备之间互联互通，达成商业成功，需要支持手机与设备之间的点到点本地控，点到点本地控制不需要云平台支持。以下简称点到点本地控。
## 3.7
**鸿蒙生态局域网本地控制 HarmonyConnect Local Network Operation**   
在鸿蒙生态系统中，设备已经完成配网，如果出现外网断开，控制设备通过路由器等设备接入家庭局域网，发送控制命令到生态设备完成控制功能。以下简称局域网本地控。

## 3.8 遵从性定义
本文针对测试例遵从性约定规范定义如下：
M：Must，必选测试特性及测试例，所有进行鸿蒙互联互通测试的设备均要遵循。  
C：Conditional，有条件必选测试特性及测试例，设备选择集成某些能力时需要遵循。   
G：General，通用规范，相关测试特性及测试例建议相关产品遵循，非强制性测试要求。   
对每个必选特性对应测试例定义的编号格式为【M-特性ID-功能点-XXXX】（例如：【M-QUERY-SERVICE-0100】）。   
对每个条件必选特性对应测试例定义的编号格式为【C-特性ID-功能点-XXXX】（例如：【C-DISCOVERY-BLE-0100】）。   
对每个通用特性对应测试例定义的编号格式为【G-特性ID-功能点-XXXX】（例如：【G-RELEASE-WIFI-0100】）。   
# 4. 符号和缩略语
下列缩略语适用于本文件。

| 缩略语 | 英文全名 | 中文解释 |
| --- | --- | --- |
| Wi-Fi | Wireless Fidelity | 无线保真 |
| SSID | Service Set Identifier | 服务集标识 |
| BLE | Bluetooth Low Energy | 蓝牙低功耗 |
| JSON | JavaScript Object Notation | JavaScript 对象表示法 |
| SPEKE | Simple Password Exponential Key Exchange | 简单密码指数密钥交换 |
| SoftAP | Soft Access Point | 软接入点 |


# 5. 互联互通互操作测试要求
鸿蒙生态设备互联互通互操作测试对象是以OpenHarmony系统为底座，集成互联互通互操作组件的生态设备，通过本文档定义的基础测试例的生态设备才能被认定为具备鸿蒙生态互联互通能力基础能力。
需要说明的是，本测试规范文档的作用时向生态设备提供已明确的鸿蒙互联互通基础能力的测试要求及测试例指导，会根据业务需求持续更新，设备开发时建议首选OpenHarmony提供的开源实现，如果要修改该首选的开源实现，请详细阅读本文档提供的基础测试要求及测试例，以确保满足鸿蒙生态互联互通的兼容性要求并遵循其规则。
在测试之前测试对象需满足以下要求：
1. 集成OpenHarmony5.0及以上版本，推荐使用最新发布LTS或Release分支的V版本。
2. 通过OpenHarmony兼容性测试及，且包括互联互通接口兼容性测试。
3. 参考本文档进行分布式功能测试时需要使用通过OpenHarmony认证的标准分布式测试工具，包括分布式测试盒子及通用互联APP，必须确保测试工具和设备间可信互联。
# 6. 设备互联互通互操作测试特性和用例
## 6.1 设备发现
特性描述：鸿蒙生态互联互通支持基于Wi-Fi、BLE及以太网等连接方式的设备发现/扫描，通过控制端设备发起设备扫描请求，设备收到请求并响应数据。互联互通提供了两种设备发现/扫描方式：列表模式及拖动模式。   
1）Wi-Fi设备发现   
Wi-Fi设备在出厂或者恢复出厂状态，上电后默认开启SoftAP，发送固定SSID格式广播帧，通用互联APP可通过Wi-Fi扫描发现设备，相关测试例如下：   

【C-DISCOVERY-WIFI-0100】Wi-Fi设备发现能力验证      
前置条件：      
（1）被测设备仅开启SoftAP功能。      
（2）被测设备上电并处于待连接状态。      
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。      
测试步骤：      
（4）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（5）点击“重新扫描设备”按钮。      
预期结果：      
（1）附近设备界面待连接设备区域或可连接设备列表显示待连接设备名称。      
   
【C-DISCOVERY-WIFI-0200】非待连接状态设备无法进行Wi-Fi设备发现能力验证   
前置条件：   
（1）被测设备未处于待连接状态，包括且不限于：   
未开启通信功能；   
被测设备为已连接状态。   
（2）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击“重新扫描设备”按钮。   
预期结果：   
（1）附近设备界面待连接设备区域或可连接设备列表不显示待连接设备名称。   
   
【C-DISCOVERY-WIFI-0300】设备断连后重新进行Wi-Fi设备发现能力验证   
前置条件：   
（1）被测设备仅开启SoftAP功能。   
（2）被测设备处于已连接状态。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）进入已连接设备详情界面，点击“断开连接”。   
（3）返回附近设备界面，点击“重新扫描设备“。   
预期结果：   
（1）被测设备断开连接后已连接设备区域/列表被测设备信息消失。   
（2）重新扫描后附近设备界面待连接设备区域或可连接设备列表显示被测设备名称。   
   
【C-DISCOVERY-WIFI-0400】不同方向Wi-Fi设备发现能力验证   
前置条件：   
（1）被测设备仅开启SoftAP功能。   
（2）被测设备上电并处于待连接状态。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）分别将被测设备放置在标准终端0°、90°、180°、270°方向。   
（3）点击“重新扫描设备”按钮。   
预期结果：   
（1）被测设备置于不同方向时附近设备界面待连接设备区域或可连接设备列表均显示被测设备名称。   
   
【C-DISCOVERY-WIFI-0500】Wi-Fi发现设备信息正确性验证   
前置条件：   
（1）被测设备仅开启SoftAP功能。   
（2）被测设备已被成功发现。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
预期结果：   
（1）附近设备界面待连接设备区域或可连接设备列表被测设备名称均正确展示。   
2）BLE设备发现   
BLE设备发现场景主要用于BLE设备在出厂状态或者恢复出厂状态时，设备上电默认开启Advertising广播的情况，此时控制端通信互联APP发起设备扫描请求，设备收到请求并相应发送响应数据，相关测试例如下：   
   
【C-DISCOVERY-BLE-0100】BLE设备发现能力验证   
前置条件：   
（1）被测设备仅开启BLE功能。   
（2）搭载通用互联APP的标准终端开启BLE功能。   
（3）被测设备上电并处于待连接状态。   
（4）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（5）进入通用互联APP附近设备页面，选择选择拖动模式或列表模式。   
（6）点击“重新扫描设备”按钮。   
预期结果：   
（1）附近设备界面待连接设备区域或可连接设备列表显示待连接设备名称。   
   
【C-DISCOVERY-BLE-0200】非待连接状态设备无法进行BLE设备发现能力验证   
前置条件：   
（1）被测设备未处于待连接状态，包括且不限于：   
未开启通信功能；   
被测设备为已连接状态。   
（2）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击“重新扫描设备”按钮。   
预期结果：   
（1）附近设备界面待连接设备区域或可连接设备列表不显示待连接设备名称。   
   
【C-DISCOVERY-BLE-0300】设备断连后重新进行BLE设备发现能力验证   
前置条件：   
（1）被测设备仅开启BLE功能。   
（2）搭载通用互联APP的标准终端开启BLE功能。   
（3）被测设备处于已连接状态。   
（4）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）进入已连接设备详情界面，点击“断开连接”。   
（3）返回附近设备界面，点击“重新扫描设备“。   
预期结果：   
（1）被测设备断开连接后已连接设备区域/列表被测设备信息消失。   
（2）重新扫描后附近设备界面待连接设备区域或可连接设备列表显示被测设备名称。   
   
【C-DISCOVERY-BLE-0400】不同方向BLE设备发现能力验证   
前置条件：   
（1）被测设备仅开启BLE功能。   
（2）搭载通用互联APP的标准终端开启BLE功能。   
（3）被测设备上电并处于待连接状态。   
（4）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）分别将被测设备放置在标准终端0°、90°、180°、270°方向。   
（3）点击“重新扫描设备”按钮。   
预期结果：   
（1）被测设备置于不同方向时附近设备界面待连接设备区域或可连接设备列表均显示被测设备名称。   
   
【C-DISCOVERY-BLE-0500】BLE发现设备信息正确性验证   
前置条件：   
（1）被测设备仅开启BLE功能。   
（2）被测设备已被成功发现。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
预期结果：   
（1）附近设备界面待连接设备区域或可连接设备列表被测设备名称均正确展示。   
   
## 6.2 设备连接   
特性描述：鸿蒙生态设备被发现后，可通过控制端设备发起设备连接。设备连接时由通用互联APP与设备进行SPEKE协商并确定PIN。相关测试例如下：   
   
【C-CONNECTION-NEGOTIATION-0100】支持默认PIN码安全协商能力验证   
前置条件：   
（1）被测设备已被发现。   
（2）设备预置默认PIN码。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
（4）被测设备未曾与标准终端连接或已删除设备重新连接。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）拖动被测设备至设备连接区域或点击可连接设备列表中的被测设备。   
预期结果：   
（1）被测设备成功连接，被测设备名称出现在已连接设备区域或已连接设备列表。   
   
【C-CONNECTION-NEGOTIATION-0200】支持弹窗录入PIN码安全协商能力验证   
前置条件：   
（1）被测设备已被发现。   
（2）被测设备为有屏设备并支持PIN码弹窗。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
（4）被测设备未曾与标准终端连接或已删除设备重新连接。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）拖动被测设备至设备连接区域或点击可连接设备列表中的被测设备。   
（3）查看被测设备弹窗显示的PIN码。   
（4）在通用互联APP弹窗输入正确PIN码。   
预期结果：   
（1）被测设备显示PIN码。   
（2）输入正确PIN码后成功连接，被测设备名称出现在已连接设备区域或已连接设备列表。   
   
【C-CONNECTION-NEGOTIATION-0300】弹窗录入错误PIN码安全协商失败能力验证   
前置条件：   
（1）被测设备已被发现。   
（2）被测设备为有屏设备并支持PIN码弹窗。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
（4）被测设备未曾与标准终端连接或已删除设备重新连接。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）拖动被测设备至设备连接区域或点击可连接设备列表中的被测设备。   
（3）查看被测设备弹窗显示的PIN码。   
（4）在通用互联APP弹窗输入错误PIN码。   
预期结果：   
（1）被测设备显示PIN码。   
（2）输入错误PIN码后通用互联APP弹出错误提示，被测设备无法连接。   
   
【C-CONNECTION-NEGOTIATION-0400】支持QR扫码录入PIN码安全协商能力验证   
前置条件：   
（1）被测设备已被发现。   
（2）被测设备带有PIN码QR码铭牌/标识。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
（4）被测设备未曾与标准终端连接或已删除设备重新连接。   
测试步骤：   
（7）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（8）拖动被测设备至设备连接区域或点击可连接设备列表中的被测设备。   
（9）通过通信互联APP扫描被测设备PIN码QR码信息。   
预期结果：   
（1）扫描QR码后成功连接，被测设备名称出现在已连接设备区域或已连接设备列表。   
   
【C-CONNECTION-NEGOTIATION-0500】扫描错误QR码安全协商失败能力验证   
前置条件：   
（1）被测设备已被发现。   
（2）被测设备带有PIN码QR码铭牌/标识。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
（4）被测设备未曾与标准终端连接或已删除设备重新连接。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）拖动被测设备至设备连接区域或点击可连接设备列表中的被测设备。   
（3）通过通信互联APP扫描非被测设备PIN码QR码信息。   
预期结果：   
（1）扫描错误QR码后通用互联APP弹出错误提示，被测设备无法连接。   
   
【C-CONNECTION-NEGOTIATION-0600】支持NFC录入PIN码安全协商能力验证   
前置条件：   
（1）被测设备已被发现。   
（2）被测设备NFC带有PIN码信息。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
（4）被测设备未曾与标准终端连接或已删除设备重新连接。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）拖动被测设备至设备连接区域或点击可连接设备列表中的被测设备。   
（3）通过搭载通信互联APP的标准终端识别被测设备NFC PIN码信息。   
预期结果：   
（1）NFC识别后成功连接，被测设备名称出现在已连接设备区域或已连接设备列表。   
   
【C-CONNECTION-NEGOTIATION-0700】支持NFC录入错误信息安全协商失败能力验证   
前置条件：   
（1）被测设备已被发现。   
（2）被测设备NFC带有PIN码信息。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
（4）被测设备未曾与标准终端连接或已删除设备重新连接。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）拖动被测设备至设备连接区域或点击可连接设备列表中的被测设备。   
（3）通过搭载通信互联APP的标准终端识别错误NFC信息。   
预期结果：   
（1）NFC识别错误信息后通用互联APP弹出错误提示，被测设备无法连接。   
   
【M-CONNECTION-CONNECTION-0200】设备断连后重新连接能力验证   
前置条件：   
（1）被测设备已连接。   
（2）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
（3）被测设备曾与标准终端连接且未被删除。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）进入已连接设备详情界面，点击“断开连接”。   
（3）返回附近设备界面，点击“重新扫描设备“。   
（4）拖动被测设备至设备连接区域或点击可连接设备列表中的被测设备。   
（5）通过默认PIN码或其它PIN码输入方式确认PIN码。   
预期结果：   
（1）被测设备成功与通用互联APP建立连接。   
   
## 6.3 信息查询   
特性描述：通用互联APP可调用相关接口查询设备服务、设备服务状态等。   
   
1）设备状态查询   
设备连接状态改变后通用互联APP会同步设备状态并展示在设备详情页。相关测试例如下：   
   
【M-QUERY-DEVICESTATUS-0100】已连接设备状态查询能力验证   
前置条件：   
（1）被测设备已连接。   
（2）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击设备连接区域被测设备详情弹窗或点击已连接设备列表中的被测设备进入设备详情页。   
预期结果：   
（1）设备状态显示为在线。   
   
【M-QUERY-DEVICESTATUS-0200】已连接设备状态切换设备状态同步能力验证   
前置条件：   
（1）被测设备已连接。   
（2）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击设备连接区域被测设备详情弹窗或点击已连接设备列表中的被测设备进入设备详情页。   
（3）被测设备下电/关闭蓝牙/断网。   
预期结果：   
（1）设备状态切换后由在线切换为离线。   
   
2）设备服务查询   
设备连接成功后通用互联APP会查询设备服务并展示在设备详情页。相关测试例如下：   
   
【M-QUERY-SERVICE-0100】已连接设备服务查询完整性验证   
前置条件：   
（1）被测设备已连接。   
（2）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击设备连接区域被测设备详情弹窗或点击已连接设备列表中的被测设备进入设备详情页。   
（3）点击典型业务，查看全部设备服务。   
预期结果：   
（1）设备详情页-典型业务展示设备可提供的全部服务，无缺失、冗余。   
   
【M-QUERY-SERVICE-0200】已连接设备服务查询正确性验证   
前置条件：   
（1）被测设备已连接。   
（2）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击设备连接区域被测设备详情弹窗或点击已连接设备列表中的被测设备进入设备详情页。   
（3）点击典型业务，查看全部设备服务。   
预期结果：   
（1）设备详情页-典型业务展示设备可提供的全部服务，各项均显示正确。   
   
3）设备服务状态查询   
设备连接成功后通用互联APP会查询设备服务并将当前服务状态展示在设备详情页。相关测试例如下：   
   
【M-QUERY-SERVICESTATUS-0100】设备服务状态手动切换同步能力验证   
前置条件：   
（1）被测设备已连接。   
（2）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击设备连接区域被测设备详情弹窗或点击已连接设备列表中的被测设备进入设备详情页。   
（3）点击典型业务，查看全部设备服务。   
（4）手动操作各服务开关或控制设备。   
预期结果：   
（1）设备详情页-典型业务-各服务项展示的服务状态与手动切换后的状态一致。   
   
【M-QUERY-SERVICESTATUS-0200】设备服务状态自动切换同步能力验证   
前置条件：   
（1）被测设备已连接。   
（2）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击设备连接区域被测设备详情弹窗或点击已连接设备列表中的被测设备进入设备详情页。   
（3）点击典型业务，查看全部设备服务。   
（4）手动操作各服务开关或控制设备。   
（5）设备下电/关闭蓝牙/断网。   
预期结果：   
（1）手动切换后设备详情页-典型业务-各服务项展示的服务状态与手动切换后的状态一致。   
（2）设备下电/关闭蓝牙/断网后，各服务项展示的服务状态自动切换为对应状态。   
   
4）设备位置信息查询   
若被测设备支持位置查询功能，连接成功后可通过通用互联APP查询设备位置信息并展示在设备详情页。相关测试例如下：   
   
【C-QUERY-LOCATIONINFO-0100】用户授权“允许本次使用”后成功获取设备位置信息能力验证   
前置条件：   
（1）被测设备已连接。   
（2）被测设备支持位置查询功能。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击设备连接区域被测设备详情弹窗或点击已连接设备列表中的被测设备进入设备详情页。   
（3）点击位置信息。   
（4）点击授权提示框“允许本次使用”按钮。   
（5）退出设备详情页面重新进入。   
预期结果：   
（1）点击设备详情页-位置信息后弹出用户授权弹框。   
（2）用户授权后可获取并展示正确设备位置信息。   
（3）退出设备详情页面并重新进入后再次弹出授权弹窗。   
   
【C-QUERY-LOCATIONINFO-0200】用户授权“仅使用期间允许”后成功获取设备位置信息能力验证   
前置条件：   
（1）被测设备已连接。   
（2）被测设备支持位置查询功能。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
（4）用户未曾授权通用互联APP获取位置信息。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击设备连接区域被测设备详情弹窗或点击已连接设备列表中的被测设备进入设备详情页。   
（3）点击位置信息。   
（4）点击授权提示框“仅使用期间允许”按钮。   
（6）退出设备详情页面重新进入。   
预期结果：   
（1）点击设备详情页-位置信息后弹出用户授权弹框。   
（2）用户授权后可获取并展示正确设备位置信息。   
（3）退出设备详情页面并重新进入后不再弹出授权弹窗，可直接获取并展示正确设备位置信息。   
   
【C-QUERY-LOCATIONINFO-0300】用户未授权无法获取设备位置信息能力验证   
前置条件：   
（1）被测设备已连接。   
（2）被测设备支持位置查询功能。   
（3）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
（4）用户未曾授权通用互联APP获取位置信息。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击设备连接区域被测设备详情弹窗或点击已连接设备列表中的被测设备进入设备详情页。   
（3）点击位置信息。   
（4）点击授权提示框“禁止”按钮。   
（5）退出设备详情页面重新进入。   
预期结果：   
（1）点击设备详情页-位置信息后弹出用户授权弹框。   
（2）用户禁止授权后不展示设备位置信息。   
（3）退出设备详情页面并重新进入后不再弹出授权弹窗，且不展示设备位置信息。   
   
## 6.4 点到点本地控   
特性描述：通用互联APP可通过点到点本地控方式向连接设备发送控制指令并接收设备执行结果返回。   
1）设备开关控制   
设备连接成功后通用互联APP可控制设备开关状态。相关测试例如下：   
   
【M-LOCALCONTROL-ON/OFF-0100】设备开关控制能力验证   
前置条件：   
（1）被测设备已连接。   
（2）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击设备连接区域被测设备详情弹窗或点击已连接设备列表中的被测设备进入设备详情页。   
（3）点击开关。   
预期结果：   
（1）被测设备关机成功，通用互联APP开关按钮及典型业务置灰，设备状态显示离线。   
   
2）设备典型业务控制   
设备连接成功后通用互联APP可对设备典型业务进行控制/操作。相关测试例如下：   
   
【M-LOCALCONTROL-SERVICE-0100】设备典型业务控制能力验证   
前置条件：   
（1）被测设备已连接。   
（2）搭载通用互联APP的标准终端与被测设备距离不大于10m且无遮挡。   
测试步骤：   
（1）进入通用互联APP附近设备页面，选择拖动模式或列表模式。   
（2）点击设备连接区域被测设备详情弹窗或点击已连接设备列表中的被测设备进入设备详情页。   
（3）点击典型业务，查看全部设备服务。   
（4）手动操作各服务开关或控制设备。   
预期结果：   
（1）被测设备正确响应各典型业务操作且设备详情页-典型业务-各服务项展示的服务状态与设备当前状态一致。
