

**1 范围**

本文件规定了鸿蒙生态富设备之间统一的投屏体验技术规范，满足不同厂商的鸿蒙生态设备之间能够互联互通投屏。本文适用于鸿蒙生态设备互联互通投屏。

**2 规范性引用文件**

下列文件中的内容通过文中的规范性引用而构成本文件必不可少的条款。其中，注日期的引用文件，仅该日期对应的版本适用于本文件；不注日期的引用文件，其最新版本（包括所有的修改单）适用于本文件。

Wi-Fi Peer-to-Peer (P2P) Technical  Specification Version1.2

Wi-Fi Display Technical Specification Version 2.2

DLNA guidelines June 2016 release



**3 术语和定义**

下列术语和定义适用于本文件。

**3.1 鸿蒙生态终端**

在鸿蒙生态系统中，连接到互联互通网络中，可以执行控制类终端的交互指令，并满足人们对环境的智能化应用需求的电子化、信息化产品。以下简称**生态终端**。

**4** **符号和缩略语**

下列缩略语适用于本文件。

| <font style="color:#000000;">缩略语</font> | <font style="color:#000000;">英文全名</font> | <font style="color:#000000;">中文解释</font> |
| --- | --- | --- |
| <font style="color:#000000;">DLNA</font> | <font style="color:#000000;">DIGITAL LIVING NETWORK ALLIANCE</font> | <font style="color:#000000;">数字生活网络联盟</font> |
| <font style="color:#000000;">DMC</font> | <font style="color:#000000;">Digital Media Controller</font> | <font style="color:#000000;">数字媒体控制器</font> |
| <font style="color:#000000;">DMP</font> | <font style="color:#000000;">Digital Media Player</font> | <font style="color:#000000;">数字媒体播放器</font> |
| <font style="color:#000000;">DMR</font> | <font style="color:#000000;">Digital Media Renderer </font> | <font style="color:#000000;">数字媒体渲染器</font> |
| <font style="color:#000000;">DMS</font> | <font style="color:#000000;">Digital Media Player</font> | <font style="color:#000000;">数字媒体服务器</font> |
| <font style="color:#000000;">UPnP</font> | <font style="color:#000000;">Universal Plug and play</font> | <font style="color:#000000;">通用即插即用</font> |
| <font style="color:#000000;">CDS</font> | <font style="color:#000000;">Content Directory Service</font> | <font style="color:#000000;">内容服务</font> |
| <font style="color:#000000;">CMS</font> | <font style="color:#000000;">Connection Manager Service</font> | <font style="color:#000000;">传输协议和媒体管理服务</font> |
| <font style="color:#000000;">AVT</font> | <font style="color:#000000;">AVTransport Service</font> | <font style="color:#000000;">多媒体播放、暂停、搜索、停止服务</font> |
| <font style="color:#000000;">RCS</font> | <font style="color:#000000;">Rendering Control Service</font> | <font style="color:#000000;">多媒体调节音量、静音等服务</font> |
| <font style="color:#000000;">SSDP</font> | <font style="color:#000000;">Simple Service Discovery Protocol</font> | <font style="color:#000000;">简单设备发现协议</font> |
| <font style="color:#000000;">SOAP</font> | <font style="color:#000000;">Simple Object Access Protocol</font> | <font style="color:#000000;">简单对象访问协议</font> |




**5 系统架构**

**5.1 系统框图**

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/95f65b9c-c2b7-4f98-b3c4-1ffe9d78ec7e/image.png 'image.png')

图1 系统架构图

投屏系统架构遵循OpenHarmony技术架构，从上到下依次为：应用层、框架层、系统服务层。投屏能力模块如图中蓝色标识。

**5.2 Miracast投屏架构图**

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/a560f4d1-e511-4621-ad3f-6bbde1c33132/image.png 'image.png')

图2 Miracast投屏架构图

**5.3 DLNA投屏架构图**

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/699d685e-a4be-43c0-a94f-84c8e847d524/image.png 'image.png')

图3 DLNA投屏架构图

**6 投屏场景**

**6.1 镜像投屏**

**6.1.1 场景综述**

镜像投屏是一种无线显示技术，可以将视频和音频从一台设备流式传输到另一台设备。本规范镜像投屏采用Miracast技术，源设备通过Wi-Fi P2P连接向显示设备发送压缩的视频和音频数据，然后，显示设备解压缩并在屏幕上显示内容。场景如下图所示：

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/c96425ce-b694-4d1f-a31a-e5de2a09099c/image.png 'image.png')

镜像投屏的主要操作步骤：

1、在SystemUI下拉框中集成无线投屏开关，通过此开关完成设备的发现、认证、连接流程。

2、投屏成功后状态栏弹出“投屏中”提示，可以断开投屏。

3、在已经投屏场景下，再次点击无线投屏重新搜索设备，支持设备切换和断连设备。

设备清单如下：

| 序号 | 品类 | 功能介绍 | 软件版本 | 环境条件 | 必选/可选 |
| --- | --- | --- | --- | --- | --- |
| 1 | 智慧屏/投屏器 | 智慧屏/投屏器支持Miracast投屏，可以接收手机、平板、PC的投屏显示 | OpenHarmony 5.0 | 电源、壁挂/摆放 | 是 |
| 2 | 手机 | 支持Miracast投屏，将手机的屏幕投屏到大屏/智 能电视上。 | OpenHarmony 5.0 | NA | 是 |
| 3 | 无线路由器 | 提供无线网络 | NA | NA | 可选 |






**6.1.2 体验UX**

6.1.2.1 发起投屏

Source端从System UI下拉菜单无线投屏开关发起投屏。

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/e2dc9dcb-cdd1-42e1-a28d-a3878b60e3d6/image.png 'image.png')



示例UX：

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/8e09c7db-c6a0-4fbd-aa60-cd66ac4095a2/image.png 'image.png')
![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/83ee9a51-2ac0-498f-bafa-4791985f09b5/image.png 'image.png')
![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/cdc24295-75ec-4873-a504-6bddc6845822/image.png 'image.png')
![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/245c3fe2-59a8-4f9b-8b5e-6faec948c7d4/image.png 'image.png')




当WiFi开关未打开时，需要开启WiFi开关，用户同意开启后，再弹出投屏搜索结果列表。

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/3a4ab952-e674-49e8-97f6-180a2e219576/image.png 'image.png')

示列UX：

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/3e70c7bc-c604-4831-af00-21031e382237/image.png 'image.png')
![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/d5bb40d4-5197-4a0e-bd30-64b7510bdf65/image.png 'image.png')
![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/ed7fcc2e-408e-431d-b453-388bc24db557/image.png 'image.png')


6.1.2.2 接受投屏

当Source端发起投屏后，在弹出界面会等待Sink端响应：

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/8053163d-54a7-4fe4-b505-cbcb68162852/image.png 'image.png')
![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/a339b17e-2361-403d-9119-53ed5a1edc43/image.png 'image.png')



UX示例：

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/9c26ead6-fb19-46b3-829e-5a4e26f0426f/image.png 'image.png')
![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/9741044d-0cae-49d6-afe2-cf3613916155/image.png 'image.png')
![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/0fc1996e-d0b7-493c-8aca-71c29a6800a2/image.png 'image.png')

6.1.2.3 结束投屏

投屏结束指令由source端发起，可以通过System UI无线投屏开关关闭，也可以通过状态栏。

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/676648e4-f417-4492-8ef6-b58716bb4ec9/image.png 'image.png')
![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/557f632d-7e5f-4640-bd75-8c5b8b791796/image.png 'image.png')



示例UX：

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/a6632701-f9a2-463f-aa21-0576e09b03ad/image.png 'image.png')
![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/eb104367-3c26-419a-8698-5c16b61a49a3/image.png 'image.png')

**6.2 内容投屏**

**6.2.1 场景综述**

内容投屏采用DLNA投屏技术。DLNA投屏是一种高效的多媒体共享技术，旨在实现个人电脑、消费电器和移动设备之间的无线和有线网络的互联互通，使得数字媒体和内容服务可以在不同设备上得到不受限制的共享。

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/09ee5907-7b5b-4d9d-b49a-0185639b7a90/image.png 'image.png')

DLNA投屏的使用步骤：

1. 确保设备支持：确认所有参与投屏的设备（手机、电脑、电视）都支持DLNA功能。
2. 连接同一局域网：将所有设备连接到同一Wi-Fi或有线网络中，并确保网络信号良好。
3. 启用DLNA功能：对于投屏目的端设备，找到网络设备或DLNA设置选项并启用DLNA共享。对于投屏源端设备，需要运行服务器软件，如电脑上的Windows Media Player，手机上的图库，或运行的在线视频APP（XX视频）。
4. 搜索并连接设备：
5. 开始投屏。

设备清单如下：

| 序号 | 品类 | 功能介绍 | 软件版本 | 环境条件 | 必选/可选 |
| --- | --- | --- | --- | --- | --- |
| 1 | 智慧屏/投屏器 | 智慧屏/投屏器支持DLNA投屏，可以接收手机、平板、PC的投屏显示 | OpenHarmony 5.1 | 电源、壁挂/摆放 | 是 |
| 2 | 手机 | 支持DLNA投屏，将手机的屏幕投屏到大屏/智 能电视上。 | OpenHarmony 5.1 | NA | 是 |
| 3 | 无线路由器 | 提供无线网络 | NA | NA | 是 |




**6.2.2 体验UX **

6.2.2.1 发起投屏

通过第三方应用投屏功能触发DLNA投屏。

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/6e079b97-3146-4075-a241-2b574b7b667c/image.png 'image.png')

6.2.2.2 接受投屏

在弹出搜索框内选择待投屏的设备，目的端大屏弹出提示界面，由用户选择接受/阻止投屏。

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/dca02396-e9d2-43f9-a9cb-e47d1aac911e/image.png 'image.png')



6.2.2.3 结束投屏

在投屏端控制界面点击关闭投屏按钮结束投屏。

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/640cbde4-dd3a-45d9-8f3c-5c50d375e903/image.png 'image.png')

**6.3 分布式投屏**

**6.3.1 场景综述**

待后续补充

**6.3.2 体验UX**

待后续补充

**7 投屏流程**

**7.1 镜像投屏流程**

Miracast 投屏业务的实现框架如下所示：

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/bd870f9f-b84c-455f-bd7c-ba3491a8640c/image.png 'image.png')



**<font style="color:#242728;">	</font>**手机向大屏设备进行Miracast投屏，按照标准的Wi-Fi P2P协议进行建链、发起WFD会话，主要流程有：

1. 手机侧点击无线投屏，调用对应的系统应用模块；
2. 调用投屏服务，对于镜像投屏场景，选择Miracast方式；
3. 调用Wi-Fi Display协议接口；
4. 调用Wi-Fi服务进行P2P建链（发现、认证、连接等）；
5. 大屏侧Wi-Fi Display协议栈调用MiracastSink服务；
6. 大屏MiracastSink服务调用显示服务进行投屏显示。



**7.2 内容投屏流程**

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/1f0cd879-83a1-4d63-956c-0d6a671d76bf/image.png 'image.png')

手机向大屏设备进行DLNA投屏，按照标准的DLNA协议进行设备发现、设备控制，主要流程有：

1. 手机侧影音应用内点击投屏，触发投屏服务；
2. 调用投屏服务，对于资源投播，选择DLNA方式；
3. DLNA服务调用UPnP协议接口；
4. 调用UPnP Device Architecture接口进行投屏连接（发现、认证、连接等），调用UPnP AV Architecture接口进行投屏控制；
5. 大屏侧UPnP协议栈调用DLNA服务；
6. 大屏DLNA服务调用显示服务进行投屏显示。



**7.3 分布式投屏流程**

待后续补充



**8 投屏接口**

**8.1 镜像投屏接口**

**8.1.1 Miracast Source端接口**

1. on.deviceFound

形式:

function on(type: 'deviceFound', callback: (deviceList: Array<CastRemoteDevice>) => void): void;

输入：void

输出：void

返回：设备列表 deviceList: Array<CastRemoteDevice>

描述：注册一个事件监听器，当发现新的设备时触发。

2. off.deviceFound

形式:

function off(type: 'deviceFound', callback?: (deviceList: Array<CastRemoteDevice>) => void): void;

输入：void

输出：void

返回：无

描述：注销一个事件监听器，停止监听发现新设备的事件。

3. startDiscovery

形式:

Function startDiscovery(protocols: Array<ProtocolType>, callback: AsyncCallback<void>): void;

function startDiscovery(protocols: Array<ProtocolType>): Promise<void>;

输入：protocols（ProtocolType数组）：要发现的协议类型数组。

输出：void

返回：无

描述：开始发现支持指定协议的设备。

4. stopDiscovery

形式:

function stopDiscovery(callback: AsyncCallback<void>): void;

function stopDiscovery():Promise<void>;

输入：void

输出：void

返回：无

描述：停止发现

5. createCastSession

形式:

function createCastSession(castSessionProperty: CastSessionProperty, callback: AsyncCallback<CastSession>): void;

function createCastSession(castSessionProperty: CastSessionProperty): Promise<CastSession>;

输入：castSessionProperty（CastSessionProperty）：创建CastSession的属性;

输出：void

返回：回调函数，返回创建的CastSession。

描述：创建一个CastSession。

6. addDevice

形式:

function addDevice(device: CastRemoteDevice): Promise<void>;

function addDevice(device: CastRemoteDevice, callback: AsyncCallback<void>): void;

输入：device（CastRemoteDevice）：要添加的设备;

输出：void

返回：无

描述：添加设备连接开始投屏。



**8.1.2 Miracast Sink端接口**

1. Cast.setDiscoverable

形式：

function setDiscoverable(isDiscoverable: boolean, callback: AsyncCallback<void>): void

function setDiscoverable(isDiscoverable:boolean): Promise<void>;

输入：isDiscoverable 是否可被发现

返回：void

描述：CastEngine接口，设置当前设备是否可被发现。

2. Cast.createCastSession

形式：

function createCastSession(castSessionProperty: CastSessionProperty, callback: AsyncCallback<CastSession>): void

function createCastSession(castSessionProperty: CastSessionProperty): Promise<CastSession>

输入：castSessionProperty CastSession属性。

返回：CastSession 投屏会话。

描述：CastEngine接口，创建一个CastSession实例。

3. CastSession.createMirrorPlayer

形式：

createMirrorPlayer(callback: AsyncCallback<MirrorPlayer>): void

createMirrorPlayer(): Promise<MirrorPlayer>

输入：无

返回：MirrorPlayer。

描述：CastSession接口，创建一个MirrorPlayer实例。

4. CastSession.release

形式：

release(callback: AsyncCallback<void>): void

release(): Promise<void>

输入：无

返回：无

描述：CastSession接口，释放当前CastSession实例。

5. MirrorPlayer.setSurface

形式：

setSurface(surfaceId: string, callback: AsyncCallback<void>): void

setSurface(surfaceId: string): Promise<void>

输入：surfaceId Surface ID

返回：无。

描述：MirrorPlayer接口，设置显示目标Surface ID。

6. MirrorPlayer.play

形式：

play(deviceId: string, callback: AsyncCallback<void>): void

play(deviceId: string): Promise<void>

输入：deviceId 当前设备ID。

返回：无

描述：MirrorPlayer接口，播放镜像投屏。

7. MirrorPlayer.pause

形式：

pause(deviceId: string, callback: AsyncCallback<void>): void

pause(deviceId: string): Promise<void>

输入：deviceId 当前设备ID。

返回：无

描述：MirrorPlayer接口，暂停镜像投屏播放。

8. MirrorPlayer.release

形式：

release(callback: AsyncCallback<void>): void

release(): Promise<void>

输入：无。

返回：无。

描述：MirrorPlayer接口，释放当前MirrorPlayer实例。

**8.2 内容投屏接口**

**8.2.1 DLNA Source端接口**

DLNA Source端主要实现DMC功能服务，为投屏应用提供了设备发现、控制，事件订阅媒体管理，内容传输功能：

(1)通过SSDP协议，实现设备发现，设备离线上线通知，完成设备搜索功能

(2)通过SOAP协议，完成control和device信息交互，实现设备的控制和响应。

(3)通过GENA协议，完成设备事件消息的订阅和推送，实现control和device的状态同步。

(4)解析sink端出传递过来的xml文件。

主要接口如下：

UpnpInit2：libupnp的初始化调用其他API之前必须要先调用此函数，单实例中只能调用一次。

UpnpSetWebServerRootDir：设置内部web服务器的文档根目录。

UpnpRegisterRootDevice，UpnpRegisterRootDevice2，UpnpRegisterRootDevice3，	        UpnpRegisterRootDevice4：四种注册设备的接口，每种传参有区别。

UpnpSendAdvertisement：发送本设备和支持的服务的发现通知。

**8.2.2 DLNA Sink端接口**

DLNA Sink端主要实现独立的DMR服务，主要功能有：

(1)在局域网中发送SSDP广播，宣告自身设备信息。

(2)监听局域网中的SSDP广播，匹配M-SEARCH搜索信息并响应。

(3)建立web服务，提供description.xml详细设备描述供source端解析。

(4)建立AVT服务，提供source端播放控制接口。

(5)建立RCS服务，提供source端渲染控制接口。

(6)建立CMS服务，提供连接管理。

(7)注册系统能力，提供大屏应用本地控制接口。

(8)解析并正确处理source端投播请求，调用系统播放器完成播控任务。

(9)支持source端与大屏本地应用双端控制和状态同步。

DLNA Sink端接口主要有三个，Dlna_StartDmr，Dlna_StopDmr，Dlna_SetDmrName。

Dlna_StartDmr：DMR服务配置为按需启动，该接口可以运行时主动拉起DMR服务。

Dlna_StopDmr:  停止DMR服务，主动释放系统资源。

Dlna_SetDmrName: 设置DMR设备的"friendly name"，即upnp设备的发现名称。

**8.3 分布式投屏接口**

待后续补充

**9 镜像投屏测试指标和方法**

**9.1 测试指标定义&技术要求**

**9.1.1 指标定义**

本规范指标覆盖投屏连接、投屏效果和流畅度。

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/848f530c-b213-4f30-ab7e-45061a66abf3/image.png 'image.png')





指标定义如下：

| 指标 | 指标定义 |
| --- | --- |
| 发现时延 | Source 发现组网内Sink设备耗时 |
| 连接时延 | Source 连接组网内Sink设备到首帧显示耗时（非首次） |
| 连接成功率 | Source与Sink设备连接成功率（非首次） |
| 中断次数 | 持续投屏过程中出现连接中断或异常退出次数 |
| 投屏时延 | Source设备与Sink设备显示时间差 |
| 投屏帧率 | Sink设备帧率 |
| 卡顿次数 | Sink设备两帧音隔时长超过100ms的次数 |




**9.1.2 技术要求**



| 技术要求 | 5G | 2.4G |
| --- | --- | --- |
| 发现时延 | 平均不高于800ms | 平均不高于900ms |
| 连接时延（非首次） | 不超过1500ms | 不超过1800ms |
| 连接成功率 | 不低于97% | 不低于95% |
| 中断次数 | 不高于1次 | 不高于1次 |
| 投屏时延 | 平均不高于150ms | 平均不高于200ms |
| 投屏帧率30fps | 不低于26fps | 不低于25fps |
| 投屏帧率60fps | 不低于48fps | 不低于46fps |
| 卡顿次数 | 不高于3次 | 不高于3次 |




**9.2 测试环境**

为保障评测结果一致性，对测试组网进行标准化要求，包括网络环境、高速相机部署要求。

1、网络环境部署要求

| 部署要求检查 | 要求 |
| --- | --- |
| 被测设备部署 | 屏蔽房 |
| 设备与AP间距 | <3m |
| 网络传输速率 | 满足投屏要求 |
| 通道使用 | 无其他设备接入AP |
| 噪声干扰 | 无其他AP信号干扰 |


2、组网部署要求

测试时使用高速相机记录测试过程，测试过程中通过高速相机同时录制Source及Sink显示效果，录制要求1080P/240fps或以上，通过分析工具统一分析输出测试结果，部署如下图参考：

![image.png](https://raw.gitcode.com/splane_liu/specification/attachment/uploads/e79cd8c0-2846-41aa-981e-fcbedc41df34/image.png 'image.png')

| 设备类型 | 数量 | 要求 |
| --- | --- | --- |
| 接收端设备 | 1 | 无 |
| 源端设备 | 1 | 无 |
| 高速相机 | 1 | 支持1080p/240fps或以上录制，同时录制接收端设备、源端设备输出 |
| AP | 1 | 同时连接接收端设备、源端设备 |
| PC（分析工具） | 1 | Windows 10(64位)，RAM 16G或以上，部署分析工具 |
| 环境模拟设备 | 2 | 安装跑流软件进行打流，模拟空口环境 |




3测试资源要求

为排除投屏资源本身影响，本规范要求投屏资源满足以下条件，建议测试时使用统一测试资源。

| 资源类型 | 特征要求 |
| --- | --- |
| 视频 | 分辨率：1080P，帧率：60fps，码率：8Mbps |
| 游戏 | 游戏帧率：60fps |
| 音频 | 48kHz 16bit |




**9.3 测试方法**

**9.3.1 发现时延**

2.4G发现时延

| 测试描述：评价2.4G网络下，发现接收端设备的时延 |
| :--- |
| 预置条件：<br/>1. 源端设备及接收端设备处于2.4G测试组网下<br/>2. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机进行视频录制<br/>2. 源端设备启动投屏设备选择界面<br/>3. 记录开始进行投屏设备搜索的时间点为T1，刷新出现接收设备时间点T2，计算设备发现时延=T2-T1<br/>4. 重复步骤1~3，执行5次<br/>5. 取平均值作为最后的结果 |


5G 发现时延

| 测试描述：评价5G网络下，发现接收端设备的时延 |
| :--- |
| 预置条件：<br/>1. 源端设备及接收端设备处于5G测试组网下<br/>2. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机进行视频录制<br/>2. 源端设备启动投屏设备选择界面<br/>3. 记录开始进行投屏设备搜索的时间点为T1，刷新出现接收设备时间点T2，计算设备发现时延=T2-T1<br/>4. 重复步骤1~3，执行5次<br/>5. 取平均值作为最后的结果 |


**9.3.2 发现成功率**

2.4G发现成功率

| 测试描述：评价2.4G网络下，源端设备发现接收端设备的成功率 |
| :--- |
| 预置条件：<br/>1. 源端设备及接收端设备处于2.4G测试组网下<br/>2. 源端设备电量>50% |
| 测试步骤：<br/>1. 源端设备启动投屏<br/>2. 源端设备弹出发现设备列表<br/>3. 记录设备列表是否成功发现接收端设备<br/>4. 重复步骤1~3，执行100次<br/>5. 计算发现成功率=（发现成功次数/发现总次数）*100% |


5G发现成功率

| 测试描述：评价5G网络下，源端设备发现接收端设备的成功率 |
| :--- |
| 预置条件：<br/>1. 源端设备及接收端设备处于5G测试组网下<br/>2. 源端设备电量>50% |
| 测试步骤：<br/>1. 源端设备启动投屏<br/>2. 源端设备弹出发现设备列表<br/>3. 记录设备列表是否成功发现接收端设备<br/>4. 重复步骤1~3，执行100次<br/>5. 计算发现成功率=（发现成功次数/发现总次数）*100% |


**9.3.3 连接时延**

2.4G连接时延

| 测试描述：评价2.4G网络下，源端设备成功连接接收端设备的时延 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于2.4G测试组网下<br/>2. 源端设备电量>50%<br/>3. 源端设备投屏发现设备列表中有接收端设备 |
| 测试步骤：<br/>1. 源端设备启动投屏，源端设备弹出发现设备列表且已出现接收端设备<br/>2. 使用高速相机同时对准源端设备和接收端设备进行录制<br/>3. 在源端设备列表中点击连接接收端设备<br/>4. 记录点击连接接收端设备时间T1，接收端设备出现连接首帧时间T2，连接时延=T2-T1<br/>5. 重复步骤1~4，执行5次<br/>6. 取平均值作为最后的结果 |


5连接时延

| 测试描述：评价5G网络下，源端设备成功连接接收端设备的时延 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于5G测试组网下<br/>2. 源端设备电量>50%<br/>3. 源端设备投屏发现设备列表中有接收端设备 |
| 测试步骤：<br/>1. 源端设备启动投屏，源端设备弹出发现设备列表且已出现接收端设备<br/>2. 使用高速相机同时对准源端设备和接收端设备进行录制<br/>3. 在源端设备列表中点击连接接收端设备<br/>4. 记录点击连接接收端设备时间T1，接收端设备出现连接首帧时间T2，连接时延=T2-T1<br/>5. 重复步骤1~4，执行5次<br/>6. 取平均值作为最后的结果 |


**9.3.4 连接成功率**

2.4G连接成功率

| 测试描述：评价2.4G网络下，源端设备连接接收端设备的成功率 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于2.4G测试组网下<br/>2. 源端设备电量>50%<br/>3. 源端设备投屏发现设备列表中有接收端设备 |
| 测试步骤：<br/>1. 源端设备启动投屏，源端设备弹出发现设备列表且已出现接收端设备<br/>2. 在源端设备列表中点击连接接收端设备<br/>3. 记录是否连接成功<br/>4. 重复步骤1~3，执行100次<br/>5. 计算连接成功率=（连接成功次数/投屏总次数）*100% |


5G连接成功率

| 测试描述：评价5G网络下，源端设备连接接收端设备的成功率 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于5G测试组网下<br/>2. 源端设备电量>50%<br/>3. 源端设备投屏发现设备列表中有接收端设备 |
| 测试步骤：<br/>1. 源端设备启动投屏，源端设备弹出发现设备列表且已出现接收端设备<br/>2. 在源端设备列表中点击连接接收端设备<br/>3. 记录是否连接成功<br/>4. 重复步骤1~3，执行100次<br/>5. 计算连接成功率=（连接成功次数/投屏总次数）*100% |


**9.3.5 中断次数**

2.4G中断次数

| 测试描述：评价2.4G网络下，源端设备连接接收端设备的中断次数 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于2.4G测试组网下<br/>2. 源端设备连接充电器<br/>3. 源端设备投屏发现设备列表中有接收端设备 |
| 测试步骤：<br/>1. 源端设备播放视频并投屏到接收端设备<br/>2. 投屏持续2小时<br/>3. 记录投屏过程中是否出现异常中断，如出现中断次数>=1次则用例Fail |


5G连接时延

| 测试描述：评价5G网络下，源端设备连接接收端设备的中断次数 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于5G测试组网下<br/>2. 源端设备连接充电器<br/>3. 源端设备投屏发现设备列表中有接收端设备 |
| 测试步骤：<br/>1. 源端设备播放视频并投屏到接收端设备<br/>2. 投屏持续2小时<br/>3. 记录投屏过程中是否出现异常中断，如出现中断次数>=1次则用例Fail |


**9.3.6 投屏时延**

2.4G投屏时延

| 测试描述：评价2.4G网络下，实时游戏投屏时延 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于2.4G测试组网下<br/>2. 源端设备及接收端设备已连接<br/>3. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机同时录制源端设备及接收端设备<br/>2. 在源端设备上打开一个支持显示毫秒时间的浮动秒表，确保该浮动秒表在源端设备画面顶层<br/>3. 源端设备打开&操作游戏，并投屏到接收端设备<br/>4. 录制1分钟投屏视频，逐帧播放录制的视频，选择接收端设备清晰展示浮动秒表中的时间的第一帧（此帧中的源端设备上的浮动秒表显示的时间也要清晰可辨），记录该帧中源端设备上显示的浮动秒表时间为T1；记录该帧中接收端设备中浮动秒表的时间为T2，计算此场景中时延为T2-T1<br/>5. 重复步骤1~4，执行5次<br/>6. 计算5次时延数据平均值 |


5G投屏时延

| 测试描述：评价5G网络下，实时游戏投屏时延 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于5G测试组网下<br/>2. 源端设备及接收端设备已连接<br/>3. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机同时录制源端设备及接收端设备<br/>2. 在源端设备上打开一个支持显示毫秒时间的浮动秒表，确保该浮动秒表在源端设备画面顶层<br/>3. 源端设备打开&操作游戏，并投屏到接收端设备<br/>4. 录制1分钟投屏视频，逐帧播放录制的视频，选择接收端设备清晰展示浮动秒表中的时间的第一帧（此帧中的源端设备上的浮动秒表显示的时间也要清晰可辨），记录该帧中源端设备上显示的浮动秒表时间为T1；记录该帧中接收端设备中浮动秒表的时间为T2，计算此场景中时延为T2-T1<br/>5. 重复步骤1~4，执行5次<br/>6. 计算5次时延数据平均值 |


**9.3.7 投屏帧率**

2.4G视频投屏帧率

| 测试描述：评价2.4G网络下，视频投屏的帧率 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于2.4G测试组网下<br/>2. 源端设备及接收端设备已连接<br/>3. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机录制接收端设备<br/>2. 源端设备播放视频并投屏到接收端设备<br/>3. 分析录制的接收端显示视频，统计1分钟平均帧率<br/>4. 重复步骤1~3，测试5次取平均值 |


5G视频投屏帧率

| 测试描述：评价5G网络下，视频投屏的帧率 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于5G测试组网下<br/>2. 源端设备及接收端设备已连接<br/>3. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机录制接收端设备<br/>2. 源端设备播放视频并投屏到接收端设备<br/>3. 分析录制的接收端显示视频，统计1分钟平均帧率<br/>4. 重复步骤1~3，测试5次取平均值 |




2.4G游戏投屏帧率

| 测试描述：评价2.4G网络下，游戏投屏的帧率 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于2.4G测试组网下<br/>2. 源端设备及接收端设备已连接<br/>3. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机录制接收端设备<br/>2. 源端设备操作游戏并投屏到接收端设备<br/>3. 分析录制的接收端显示视频，统计1分钟平均帧率<br/>4. 重复步骤1~3，测试5次取平均值 |




5G游戏投屏帧率

| 测试描述：评价5G网络下，游戏投屏的帧率 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于5G测试组网下<br/>2. 源端设备及接收端设备已连接<br/>3. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机录制接收端设备<br/>2. 源端设备操作游戏并投屏到接收端设备<br/>3. 分析录制的接收端显示视频，统计1分钟平均帧率<br/>4. 重复步骤1~3，测试5次取平均值 |


**9.3.8 投屏流畅性**

2.4G视频投屏卡顿次数

| 测试描述：评价2.4G网络下，视频投屏中出现的卡顿次数 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于2.4G测试组网下<br/>2. 源端设备及接收端设备已连接<br/>3. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机录制接收端设备<br/>2. 源端设备播放视频并投屏到接收端设备<br/>3. 持续投屏5分钟，分析接收端显示视频，记录接收端设备连续的视频帧间隔时长<br/>4. 统计5分钟内，帧间隔时长超过100ms的次数，即为卡顿次数 |


5G视频投屏卡顿次数

| 测试描述：评价5G网络下，视频投屏中出现的卡顿次数 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于5G测试组网下<br/>2. 源端设备及接收端设备已连接<br/>3. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机录制接收端设备<br/>2. 源端设备播放视频并投屏到接收端设备<br/>3. 持续投屏5分钟，分析接收端显示视频，记录接收端设备连续的视频帧间隔时长<br/>4. 统计5分钟内，帧间隔时长超过100ms的次数，即为卡顿次数 |


2.4G游戏投屏卡顿次数

| 测试描述：评价2.4G网络下，游戏投屏中出现的卡顿次数 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于2.4G测试组网下<br/>2. 源端设备及接收端设备已连接<br/>3. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机录制接收端设备<br/>2. 源端设备操作游戏并投屏到接收端设备<br/>3. 持续投屏5分钟，分析接收端显示视频，记录接收端设备连续的视频帧间隔时长<br/>4. 统计5分钟内，帧间隔时长超过100ms的次数，即为卡顿次数 |




5G游戏投屏卡顿次数

| 测试描述：评价5G网络下，游戏投屏中出现的卡顿次数 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于5G测试组网下<br/>2. 源端设备及接收端设备已连接<br/>3. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机录制接收端设备<br/>2. 源端设备操作游戏并投屏到接收端设备<br/>3. 持续投屏5分钟，分析接收端显示视频，记录接收端设备连续的视频帧间隔时长<br/>4. 统计5分钟内，帧间隔时长超过100ms的次数，即为卡顿次数 |






5G视频投屏卡顿次数

| 测试描述：评价5G网络下，视频投屏中出现的卡顿次数 |
| --- |
| 预置条件：<br/>1. 源端设备及接收端设备处于5G测试组网下<br/>2. 源端设备及接收端设备已连接<br/>3. 源端设备电量>50% |
| 测试步骤：<br/>1. 使用高速相机录制接收端设备<br/>2. 源端设备播放视频并投屏到接收端设备<br/>3. 持续投屏5分钟，分析接收端显示视频，记录接收端设备连续的视频帧间隔时长<br/>4. 统计5分钟内，帧间隔时长超过100ms的次数，即为卡顿次数 |




