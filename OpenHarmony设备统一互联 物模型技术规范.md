# 1 范围
本文件规定了以OpenHarmony为系统底座的生态设备的物模型关键设计元素、元素间逻辑配套关系、主要设计原则及元素表达方式等内容。  
本文件适用于以OpenHarmony为系统底座的各种消费品类设备及各行业生产经营设备的物模型实例化呈现。  
# 2 规范性引用文件
下列文件中的内容通过文中的规范性引用而构成本文件必不可少的条款。其中，注日期的引用文件，仅该日期对应的版本适用于本文件；不注日期的引用文件，其最新版本（包括所有的修改单）适用于本文件。  
T/GIIC XXXXX—XXXX 鸿蒙生态设备互联互通互操作接入与控制接口规范
# 3 术语和定义
下列术语和定义适用于本文件。
## 3.1  物模型Thing model
“物模型”，又可称设备模型或数据模型，是对物理设备进行抽象建模后在数字化空间的表达形式。
## 3.2  元素Element
元素泛指用于描述设备物模型的各类组成部分，在本文件中包括服务、事件、特征等。
## 3.3  特征 Characteristic
特征是指设备具备的某一种能力或当前所处的某个状态。某个特征可以是只读的，即其值不能修改；也可以是可写的，即能被设置为新值。
## 3.4  服务Service
服务是由一组特征元素构成的表示设备某个组成部分的逻辑或物理单元。
## 3.5  事件Event
事件是设备运行期间在某种条件下主动上报的特定类型信息。一般包含需要被外部感知和处理的通知信息，如故障、告警等。
# 4 符号和缩略语
下列缩略语适用于本文件。  
ID 身份标识（Identifier）  
sid 服务实例标识（service instance ID）

# 5 物模型组成元素与逻辑关系
本章节给出了OpenHarmony设备描述自身能力所需的基本组成元素及彼此间的逻辑关系。如图1所示，用于描述设备物模型的基本组成元素包含服务、事件、特征以及一些“关键信息”。  
![](image/物模型图1.png)  
图1 物模型组成元素及逻辑关系图  

结合图1所示，下面逐一介绍各个组成元素的具体含义及相关约束。  
1） 设备关键信息涵盖的具体内容可参见表1描述  
                                表1 设备关键信息字段列表

| **名称** | **英文名称** | **数据类型** | **最大长度/固定长度** | **是否必选** | **说明** |
| --- | --- | --- | --- | --- | --- |
| 设备型号ID | prodId | String | 固定长度：5个字节 | 必选 | 对于一款设备来讲，该ID是其在整个OpenHarmony生态产品体系内的唯一标识。其值由设备物模型运营主体统一分配。长度为5个字符。其首字符当前默认取值为1，即“1XXXX”。而其余的后4个字符，每个字符的取值范围是[0-9]或[A-Z]。理论上可提供1679616种取值选项。 |
| 设备名称 | deviceName | String | 最大长度：255个字节 | 必选 | 通过字符串表示。最长255个字节。 |
| 设备认证型号 | deviceModel | String | 最大长度：32个字节 | 必选 | 设备厂商用于标识不同型号设备的一段字符串。例如某厂商手表的一些设备型号有GTN-B19、ESA-D20。其长度最长为32个字节 |
| 设备品类ID | deviceTypeId | String | 固定长度：4个字节 | 必选 | 该ID代表设备的所属种类。其值由设备物模型运营主体统一分配。长度为4个字符。为了与现有OpenHarmony生态设备保持兼容，其首字符默认取值为0，即“0XXX”；而其余的后3个字符，每个字符的取值范围是[0-9]或[A-Z]。理论上可提供46655种取值选项。 |
| 设备品类名称 | deviceTypeName | String | 最大长度：255个字节 | 必选 | 对应于上一行的“设备品类ID”，该信息表示设备种类名称。最长255个字节。 |
| 设备制造商ID | manufacturerId | String | 固定长度：3个字节 | 必选 | 该ID是设备制造商在整个OpenHarmony生态产品体系内的唯一标识。长度固定为3个字节。其取值与OpenHarmony社区XTS认证时分配给厂商的ManufactureID值保持一致。 |
| 设备制造商名称 | manufacturerName | String | 最大长度：255个字节 | 必选 | 对应于上一行的“设备制造商ID”，该信息表示设备制造商名称。最长255个字节。 |


2）除了“设备关键信息”之外，设备物模型还可包含其他两类元素，即“服务”与“事件”。“服务”代表了设备的某个组成部分，可以表示一个独立的逻辑或物理业务单元。其中包含了外部可访问的相关信息或能力。设备物模型中至少应提供一类“服务”；“事件”是设备运行期间在某种条件下主动推送或上报到外部的特定类型信息。在设备物模型中可以不提供任何事件，也可以提供多个事件。  
3）服务是物模型模块化设计理念的体现。同一服务类型在多个不同设备对象中“可复用”。另外，在一个设备对象中可以包含同一服务类型的多个实例。  
4）“特征”是“服务”及“事件”对外呈现信息或体现具体能力的基本承载单元。外部可以读取某个特征携带的数据来获得设备的相关状态，也能通过修改某个特征的取值来驱动设备执行某个动作。一类“服务”至少应该包含一个“特征”；一种“事件”可以不含任何“特征”，也可以包含多个“特征”。  
# 6 物模型组成元素定义及实例化
## 6.1 特征的字段定义
如前所述，“特征”是“服务”及“事件”对外呈现信息或体现具体能力的基本承载单元。本章节通过表2说明描述一个特征所需涵盖的信息字段。  
表2 特征的描述字段

| **字段** | **英文名称** | **是否必选** | **说明** |
| --- | --- | --- | --- |
| 特征名称 | characteristicName | 必选 | 本字段通过字符串表述。最长占用128个字节。是该特征在整个物模型特征集合中的唯一标识。 |
| 特征数据类型 | characteristicType | 必选 | 此数据类型通过字符串来表述。最长32个字节。比如整型值用“int”表示，枚举值用“enum”表示。现有数据类型列表请参见表3； |
| 读写标识 | method | 必选 | 此字段代表在设备互操作过程中特征值是否可被读取或修改。目前取值范围如下：“R”：特征值是只读的；“W”：特征值是只可写的；“RW”：可读可写；|
| 最大值 | max | 可选 | 当特征数据类型为数值型（比如int/float等）时此字段有效； |
| 最小值 | min | 可选 | 当特征数据类型为数值型（比如int/float等）时此字段有效； |
| 步进值 | Step | 可选 | 当特征数据类型为数值型（比如int/float等）时此字段有效；表示特征值发生变化时的最小变化量 |
| 小数部分位数 | decimalDigits | 可选 | 当特征数据类型为float型时此字段有效；表示小数点后的位数，即精度 |
| 最大长度 | maxLength | 可选 | 当特征数据类型为string型时此字段有效；表示字符串的最大长度 |
| 枚举取值列表 | enumList | 可选 | 当特征数据类型为枚举型或数值型时此字段有效；枚举出各个可能取值。Enumlist列表元素的结构体定义参见表4 |
| 单位 | unit | 可选 | 关于数值单位的描述，如℃，cm，kg等；当数据类型为数值型时有效。当前可选取值参见表5 |





表3 数据类型列表

| **数据类型** | **说明** |
| --- | --- |
| bool | 布尔型值 |
| int32 | 4字节整型值 |
| uint32 | 4字节无符号整型值 |
| uint8 | 1字节无符号整型值 |
| uint16 | 2字节无符号整型值 |
| enum | 枚举型 |
| float  | 浮点数 |
| string  | 字符串 |
| 扩展预留 | 
 |


表4 Enumlist列表元素的结构体定义

| **字段** | **英文名称** | **是否必选** | **说明** |
| --- | --- | --- | --- |
| 枚举值 | enumVal | 必选 | 既可以是整型值，也可以是字符串。最长不超过16字节。 |
| 枚举值的中文含义 | descCh | 可选 | 通过字符串类型表述。最长128个字节。 |
| 枚举值的英文含义 | descEn | 可选 | 通过字符串类型表述。最长128个字节。 |


表5 数值单位列表

| **单位名称** | **对应的符号值** |
| --- | --- |
| 百分比 | % |
| 秒 | s |
| 毫秒 | ms |
| 分钟 | min |
| 小时 | hour |
| 天 | day |
| 月 | month |
| 年 | year |
| 米 | m |
| 厘米 | cm |
| 毫米 | mm |
| 千米 | km |
| 摄氏度 | ℃ |
| 华氏度 | ℉ |
| 千克 | kg |
| 克 | g |
| 速度（米/秒） | m/s |
| 速度（千米/小时） | km/h |
| 扩展预留 | 
 |


## 6.2 服务的字段定义
如前所述，“服务”是由一组“特征”构成的表示设备某个组成部分的逻辑或物理单元。本章节通过表6说明描述一类服务所需涵盖的信息字段。
表6 服务的描述字段列表

| **字段** | **英文名称** | **是否必选** | **数据类型** | **最大长度** | **说明** |
| --- | --- | --- | --- | --- | --- |
| 服务类型标识 | serviceType | 必选 | String | 64 | 在OpenHarmony生态体系内唯一标识该服务类型。 |
| 服务实例标识 | serviceId | 必选 | String | 32 | 用于在具体设备对象内唯一标识一个服务实例。通过此标识，外部在访问设备时可以在报文中指定要读取或修改的特征信息隶属于哪一个服务实例。 |
| 特征列表 | characteristics | 必选 | | | 以列表形式罗列出该服务类型所支持的所有特征。其中包含必选特征及可选特征。而描述某个特征的技术要求请参见前述表2定义。 |


## 6.3 事件的字段定义
如前所述，“事件”是设备运行期间在某种条件下主动推送或上报的特定类型信息。本章节通过表7说明描述一种事件所需涵盖的信息字段。
表7 事件的描述字段列表

| **字段** | **英文名称** | **是否必选** | **数据类型** | **最大长度** | **说明** |
| --- | --- | --- | --- | --- | --- |
| 事件类型标识 | eventType | 必选 | String | 64 | 在OpenHarmony生态体系内唯一标识该事件类型。 |
| 事件实例标识 | eventId | 必选 | String | 32 | 用于在具体设备对象内唯一标识一个事件实例。通过此标识，外部在接收到该事件后可识别出具体事件含义或来源。 |
| 特征列表 | characteristics | 可选 | | | 以列表形式罗列出该事件类型所支持的所有特征。其中包含必选特征及可选特征。而描述某个特征的技术要求请参见前述表2定义。 |


## 6.4 设备物模型实例化
基于前述章节对物模型组成元素的定义及相应规则，我们可以把一个设备的能力全貌描述出来。本文件建议采用Json格式来承载能力全貌的描述结果。前述表2、表4、表6、表7中的“英文名称”列即可作为相应字段的Json标签名。下面是某款洗衣机设备的物模型实例化Json样例。供其他设备参考。
```json
{
  "prodId": "015CD",
  "deviceModel": "HBL-19",
  "deviceTypeId": "0052",
  "deviceTypeName": "洗衣机",
  "deviceTypeNameEn": "washer",
  "deviceName": "XXX洗衣机",
  "manufacturerId": "061",
  "manufacturerName": "XXXXXX",
  "services": [
    {
      "serviceId": "switch",
      "serviceType": "binarySwitch",
      "serviceName": "开关",
      "serviceNameEn": "onoff",
      "characteristics": [
        {
          "characteristicName": "on",
          "characteristicType": "enum",
          "method": "RW",
          "enumList": [
            {
              "enumVal": "0",
              "descCh": "关",
              "descEn": "off"
            },
            {
              "enumVal": "1",
              "descCh": "开",
              "descEn": "on"
            }
          ]
        }
      ]
    },
    {
      "serviceId": "washer",
      "serviceType": "washer",
      "serviceName": "清洗",
      "serviceNameEn": "washer",
      "characteristics": [
        {
          "characteristicName": "program",
          "characteristicType": "enum",
          "method": "RW",
          "enumList": [
            {
              "enumVal": "1",
              "descCh": "ECO经济洗/节能",
              "descEn": ""
            },
            {
              "enumVal": "2",
              "descCh": "快洗",
              "descEn": ""
            },
            {
              "enumVal": "3",
              "descCh": "混合洗",
              "descEn": ""
            },
            {
              "enumVal": "4",
              "descCh": "超净洗",
              "descEn": ""
            },
            {
              "enumVal": "5",
              "descCh": "筒自洁",
              "descEn": ""
            },
            {
              "enumVal": "6",
              "descCh": "单脱水",
              "descEn": ""
            }
          ]
        }
      ]
    },
    {
      "serviceId": "faultDetection",
      "serviceType": "faultDetection",
      "serviceName": "故障检测",
      "serviceNameEn": "Fault detection",
      "characteristics": [
        {
          "characteristicName": "status",
          "characteristicType": "enum",
          "method": "R",
          "enumList": [
            {
              "enumVal": "0",
              "descCh": "运行正常，无错误",
              "descEn": ""
            },
            {
              "enumVal": "1",
              "descCh": "设备运行异常",
              "descEn": ""
            }
          ]
        }
      ]
    },
    {
      "serviceId": "update",
      "serviceType": "devOta",
      "serviceName": "软件升级",
      "serviceNameEn": "Software Update",
      "characteristics": [
        {
          "characteristicName": "action",
          "characteristicType": "enum",
          "method": "W",
          "enumList": [
            {
              "enumVal": "0",
              "descCh": "检查升级",
              "descEn": ""
            },
            {
              "enumVal": "1",
              "descCh": "启动升级",
              "descEn": ""
            }
          ]
        },
        {
          "characteristicName": "version",
          "characteristicType": "string",
          "maxLength": 64,
          "method": "R"
        },
        {
          "characteristicName": "introduction",
          "characteristicType": "string",
          "maxLength": 1024,
          "method": "R"
        },
        {
          "characteristicName": "progress",
          "characteristicType": "uint8",
          "method": "R",
          "unit": "%"
        },
        {
          "characteristicName": "bootTime",
          "characteristicType": "uint",
          "method": "R",
          "unit": "s"
        }
      ]
    }
  ]
}
```
## 6.5 物模型元素在通信报文中的应用
如下两个章节以Coap协议为例，说明特征/服务等关键元素在实例化应用过程中的使用方法。针对具体控制接口相关的详细报文信息，请参见《Openharmony设备互联互通互操作 接入与控制接口规范》的对应章节。
1）获取单个服务实例相关特征信息的请求报文如下：
GET /{serviceId}
对应的响应报文如下：
```json
{
  “{characteristicName}”: “value1”,
  “{characteristicName}”: “value2”,
  ……
}
```
修改单个服务实例相关特征信息的请求报文如下：
```json
POST /{serviceId}
{
  “{characteristicName}”: "value1",
  “{characteristicName}”: "value2",
  ……
}
```
2）修改多个服务实例相关特征信息的请求报文如下：
```json
POST /batchCmd
{
  ……
  “actions": [
    {
      "sid": “{serviceId}”,
      "data": {
        "{characteristicName}": "value1",
        ……
      }
    },
    {
      "sid": “{serviceId}”,
      "data": {
        "{characteristicName}": "value2",
        ……
      }
    }
  ]
} 
```

# 7 基础特征列表
表8 基础特征列表

| **特征名称** | **特征数据类型** | **读写标识** | **最大值** | **最小值** | **步进值** | **小数部分位数** | **最大长度** | **枚举取值列表** | **取值单位** | **特征说明** |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| productionDate | string | R |  |  |  |  | 128 |  |  | 设备制造日期 |
| brand | enum | R |  |  |  |  | | 待运营主体统一协调分配 |  | 设备品牌名 |
| deviceId | string | R |  |  |  |  | 128 |  |  | 设备在注册到云后分配的唯一标识 |
| ethMac | string | R/W | | | | | 32 | | | 以太网卡mac地址 |
| btMac | string | R/W | | | | | 32 | | | 蓝牙mac地址 |
| wifiMac | string | R/W | | | | | 32 | | | Wifi mac地址 |
| os.version | string | R |  |  |  |  | 64 |  |  | 操作系统软件的版本号 |
| os.type | enum | R | | | | | | 待运营主体统一协调分配 | | 操作系统类型 |
| hardware.version | string | R |  |  |  |  | 64 |  |  | 硬件版本号（是指主板的版本号） |
| firmware.version | string | R |  |  |  |  | 64 |  |  | 固件版本号(是指嵌入式设备的系统软件版本) |
| SerialNumber | string | R |  |  |  |  | 128 |  |  | 设备序列号 |
| on | enum | R/W |  |  |  |  |  | 0：关闭 1：打开 |  | 表示“打开”和“关闭”两种相反动作或状态。既可以指代设备的开关机，也可以标识设备上某开关器件的开合两种状态|
| restart | enum | R/W | | | | | | 0：重启1：休眠 | | 表示某个设备或系统执行重启或休眠动作 |
| peerConnectStatus | enum | R |  |  |  |  |  | 0：设备在线 1：设备离线 |  | 设备当前的网络状态信息。主要包括设备是否处于在网状态等 |
| latitude | string | R |  |  |  |  | 20 |  |  | 维度 |
| longitude | string | R |  |  |  |  | 20 |  |  | 经度 |
| RSSI | int32 | R |  |  |  |  |  |  | dbm | 无线信号强度 |
| SSID | string | R |  |  |  |  | 50 |  |  | 网络接入点名称 |
| ipv4Addr | string | R |  |  |  |  | 20 |  |  | 设备的ipv4地址 |
| ipv6Addr | string | R |  |  |  |  | 20 |  |  | 设备的ipv6地址 |
| fault.code | Enum | R | | | | | | 0：正常状态 XX：后续值待定，目前可自行扩展 | | 设备异常状态码 |
| fault.message | String | R | | | | | 512 | | | 设备异常信息描述 |
| cpu.model | string | R | | | | | 128 | | | CPU型号 |
| cpu.manufacturer | string | R | | | | | 64 | | | CPU制造商 |
| cpu.currentUsage | float | R | 100 | 0 | | 1 | | | % | CPU占用率 |
| cpu.usageThreshold | float | R/W | 100 | 0 | | 1 | | | % | CPU最大占用率 |


# 8 基础服务列表

| **服务类型标识** | **类型说明** | **特征列表** | **特征是否必选** |
| --- | --- | --- | --- |
| devInfo | 设备相关信息 | productionDate | 可选 |
|  |  | brand | 可选 |
|  |  | deviceID | 可选 |
|  |  | serialNumber | 可选 |
|  |  | peerConnectStatus | 可选 |
| ethInfo | 以太网服务相关信息 | ethMac | 必选 |
|  |  | ipv4Addr | 可选 |
|  |  | ipv6Addr | 可选 |
| wifiInfo | Wifi服务相关信息 | wifiMac | 必选 |
|  |  | ipv4Addr | 可选 |
|  |  | ipv6Addr | 可选 |
|  |  | RSSI | 可选 |
|  |  | SSID | 可选 |
| btInfo | 蓝牙服务相关信息 | btMac | 必选 |
| os | OS基本信息 | version | 必选 |
|  |  | type | 可选 |
| cpu | Cpu基本信息 | manufacturer | 必选 |
|  |  | model | 必选 |
|  |  | currentUsage | 可选 |
|  |  | usageThreshold | 可选 |
| gps | GPS服务相关信息 | latitude | 必选 |
|  |  | longitude | 必选 |
| switch | 开关服务 | on | 必选 |
| restart | 设备重启服务 | restart | 必选 |



# 9  基础事件列表
| **事件类型标识** | **类型说明** | **特征列表** | **特征是否必选** |
| --- | --- | --- | --- |
| faultEvt | 故障上报事件 | fault.code | 必选 |
|  |  | fault.message | 可选 |

# 10 设备扩展能力的定义与描述
第7至第9章节给出了描述一个设备所需的基础能力集合。但在实际场景中很多设备自身都会具备一些特色化或差异化的能力。本章节提供了一种面向差异化能力的扩展描述机制。

针对设备差异化能力，无论是特征、服务还是事件，设备厂商需要在自行定义的“特征名称”、“服务类型标识”或“事件类型标识”前新增前缀字符串“cust.”以进行显性声明。

以某厂商数字标牌设备为例，其中“文件分享”属于独有特色能力。针对该能力的服务及特征相关定义可参考下表：



| **<font style="color:rgb(0,0,0);">特征名称</font>** | **<font style="color:rgb(0,0,0);">特征数据类型</font>** | **<font style="color:rgb(0,0,0);">读写标识</font>** | **<font style="color:rgb(0,0,0);">最大值</font>** | **<font style="color:rgb(0,0,0);">最小值</font>** | **<font style="color:rgb(0,0,0);">步进值</font>** | **小数部分位数** | **最大长度** | **<font style="color:rgb(0,0,0);">枚举取值列表</font>** | **<font style="color:rgb(0,0,0);">取值单位</font>** | **<font style="color:rgb(0,0,0);">特征说明</font>** |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| <font style="color:rgb(0,0,0);">cust.shareFileList</font> | <font style="color:rgb(0,0,0);">string</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | | | | | <font style="color:rgb(0,0,0);">1</font><font style="color:rgb(0,0,0);">000</font> | | | <font style="color:rgb(0,0,0);">共享文件列表</font> |
| <font style="color:rgb(0,0,0);">cust.sendFile</font> | <font style="color:rgb(0,0,0);">enum</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | | | | | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">0：启动分享</font><font style="color:rgb(0,0,0);">   </font><font style="color:rgb(0,0,0);">1：停止分享</font> | | <font style="color:rgb(0,0,0);">控制文件分享过程的相关指令</font> |




| **<font style="color:rgb(0,0,0);">服务类型标识</font>** | **<font style="color:rgb(0,0,0);">类型说明</font>** | **<font style="color:rgb(0,0,0);">特征列表</font>** | **<font style="color:rgb(0,0,0);">特征是否必选</font>** |
| --- | :--- | :--- | :--- |
| <font style="color:rgb(0,0,0);">cust.shareFile</font> | <font style="color:rgb(0,0,0);">描述文件分享的能力</font> | <font style="color:rgb(0,0,0);">cust.shareFileList</font> | <font style="color:rgb(0,0,0);">必选</font> |
| | | <font style="color:rgb(0,0,0);">cust.sendFile</font> | <font style="color:rgb(0,0,0);">必选</font> |




另外在厂商自定义的“服务类型”中，既可以包含自定义的特征列表，也可以包括第7章节中的基础特征列表。


# 附 录 A
（规范性）
设备实例化  
A.1 数字标牌

| **服务类型标识** | **类型说明** | **特征列表** |
| --- | --- | --- |
| wifi | Wifi服务相关信息 | mac |
| | | ip |
| | | rssi |
| | | ssid |
| os | OS基本信息 | version |
| cpu | CPU基本信息 | manufacturer |
| | | model |
| | | currentUsage |
| | | usageThreshold |
| switch | 开关服务 | on |
| restart | 设备重启服务 | restart |
| cust.shareFile | 文件分享服务 | cust.shareFileList |
| | | cust.sendFile |




| **<font style="color:rgb(0,0,0);">特征名称</font>** | **<font style="color:rgb(0,0,0);">特征数据类型</font>** | **<font style="color:rgb(0,0,0);">读写标识</font>** | **<font style="color:rgb(0,0,0);">最大值</font>** | **<font style="color:rgb(0,0,0);">最小值</font>** | **<font style="color:rgb(0,0,0);">步进值</font>** | **小数部分位数** | **最大长度** | **<font style="color:rgb(0,0,0);">枚举取值列表</font>** | **<font style="color:rgb(0,0,0);">取值单位</font>** | **<font style="color:rgb(0,0,0);">特征说明</font>** |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| <font style="color:rgb(0,0,0);">cust.shareFileList</font> | <font style="color:rgb(0,0,0);">string</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | | | | | <font style="color:rgb(0,0,0);">1</font><font style="color:rgb(0,0,0);">000</font> | | | <font style="color:rgb(0,0,0);">共享文件列表</font> |
| <font style="color:rgb(0,0,0);">cust.sendFile</font> | <font style="color:rgb(0,0,0);">enum</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | | | | | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">0：启动分享</font><font style="color:rgb(0,0,0);">   </font><font style="color:rgb(0,0,0);">1：停止分享</font> | | <font style="color:rgb(0,0,0);">控制文件分享过程的相关指令</font> |






A.2 隧道控制器

| **服务类型标识** | **类型说明** | **特征列表** |
| --- | --- | --- |
| <font style="color:rgb(0,0,0);">ethInfo</font> | 以太网服务相关信息 | mac |
| | | ip |
| os | OS基本信息 | version |
| Cpu | CPU基本信息 | manufacturer |
| | | model |
| | | currentUsage |
| | | usageThreshold |
| RAM | 内存相关信息 | currentUsage |
| | | usageThreshold |
| | | cust.totalMemory |
| ROM | ROM存储器相关信息 | currentUsage |
| | | usageThreshold |
| | | cust.totalStorage |
| Cust.tunnelLocation | 隧道设备位置服务 | Cust.tunnelNum |
| | | cust.tunnelCode |
| | | cust.roadCode |
| | | cust.contructionStakeNum |
| cust.subDeivceList | 下挂设备的相关信息 | Cust.subDevNum |
| | | Cust.subDevList |
| cust.shareFile | 文件分享服务 | cust.shareFileList |
| | | cust.sendFile |
| faultStatus | 异常状态服务 | code |
| | | <font style="color:rgb(0,0,0);">message</font> |




| **<font style="color:rgb(0,0,0);">特征名称</font>** | **<font style="color:rgb(0,0,0);">特征数据类型</font>** | **<font style="color:rgb(0,0,0);">读写标识</font>** | **<font style="color:rgb(0,0,0);">最大值</font>** | **<font style="color:rgb(0,0,0);">最小值</font>** | **<font style="color:rgb(0,0,0);">步进值</font>** | **小数部分位数** | **最大长度** | **<font style="color:rgb(0,0,0);">枚举取值列表</font>** | **<font style="color:rgb(0,0,0);">取值单位</font>** | **<font style="color:rgb(0,0,0);">特征说明</font>** |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| <font style="color:rgb(0,0,0);">cust.totalMemory</font> | <font style="color:rgb(0,0,0);">Float</font> | <font style="color:rgb(0,0,0);">R</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">3</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">M</font><font style="color:rgb(0,0,0);">B</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">AM存储空间大小</font> |
| <font style="color:rgb(0,0,0);">cust.totalStorage</font> | <font style="color:rgb(0,0,0);">Float</font> | <font style="color:rgb(0,0,0);">R</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">3</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">GB</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">OM存储空间大小</font> |
| <font style="color:rgb(0,0,0);">cust.tunnelNum</font> | <font style="color:rgb(0,0,0);">String</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">2</font><font style="color:rgb(0,0,0);">0</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">隧道洞编号</font> |
| <font style="color:rgb(0,0,0);">cust.tunnelCode</font> | <font style="color:rgb(0,0,0);">String</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">2</font><font style="color:rgb(0,0,0);">0</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">隧道编号</font> |
| <font style="color:rgb(0,0,0);">cust.roadCode</font> | <font style="color:rgb(0,0,0);">String</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">2</font><font style="color:rgb(0,0,0);">0</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">路段编号</font> |
| <font style="color:rgb(0,0,0);">cust.contructionStakeNum</font> | <font style="color:rgb(0,0,0);">String</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">2</font><font style="color:rgb(0,0,0);">0</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">设备桩号</font> |
| <font style="color:rgb(0,0,0);">cust.subDevList</font> | <font style="color:rgb(0,0,0);">string</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | | | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">下挂设备列表</font><font style="color:rgb(0,0,0);">。</font><font style="color:rgb(0,0,0);">具体格式如下</font><font style="color:rgb(0,0,0);">：</font><br/><font style="color:rgb(0,0,0);">“</font><font style="color:rgb(0,0,0);">{</font><font style="color:rgb(0,112,192);">devID1,prodId1</font><font style="color:rgb(0,0,0);">};</font><br/><font style="color:rgb(0,0,0);">{</font><font style="color:rgb(0,0,0);">devID2,prodId2</font><font style="color:rgb(0,0,0);">};</font><br/><font style="color:rgb(0,0,0);">{</font><font style="color:rgb(0,0,0);">……</font><font style="color:rgb(0,0,0);">}</font><font style="color:rgb(0,0,0);">”</font> |
| <font style="color:rgb(0,0,0);">cust.subDevNum</font> | <font style="color:rgb(0,0,0);">Uint16</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | | | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">当前下挂设备的数量</font> |




A.3 隧道洞外光强检测仪

| **服务类型标识** | **类型说明** | **特征列表** |
| --- | --- | --- |
| cust.brightness | 发光强度服务 | brightness |
| switch | 开关服务 | on |
| faultStatus | 异常状态服务 | code |
| | | <font style="color:rgb(0,0,0);">message</font> |




| **<font style="color:rgb(0,0,0);">特征名称</font>** | **<font style="color:rgb(0,0,0);">特征数据类型</font>** | **<font style="color:rgb(0,0,0);">读写标识</font>** | **<font style="color:rgb(0,0,0);">最大值</font>** | **<font style="color:rgb(0,0,0);">最小值</font>** | **<font style="color:rgb(0,0,0);">步进值</font>** | **小数部分位数** | **最大长度** | **<font style="color:rgb(0,0,0);">枚举取值列表</font>** | **<font style="color:rgb(0,0,0);">取值单位</font>** | **<font style="color:rgb(0,0,0);">特征说明</font>** |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| <font style="color:rgb(0,0,0);">brightness</font> | <font style="color:rgb(0,0,0);">float</font> | <font style="color:rgb(0,0,0);">R</font> | <font style="color:rgb(0,0,0);"></font> | | | <font style="color:rgb(0,0,0);">2</font> | <font style="color:rgb(0,0,0);"></font> | | <font style="color:rgb(0,0,0);">Cd/m2</font> | <font style="color:rgb(0,0,0);">发光强度值</font> |




A.4 隧道洞内照度检测仪

| **服务类型标识** | **类型说明** | **特征列表** |
| --- | --- | --- |
| cust.illuminance | 光照强度服务 | illuminance |
| switch | 开关服务 | on |
| faultStatus | 异常状态服务 | code |
| | | <font style="color:rgb(0,0,0);">message</font> |




| **<font style="color:rgb(0,0,0);">特征名称</font>** | **<font style="color:rgb(0,0,0);">特征数据类型</font>** | **<font style="color:rgb(0,0,0);">读写标识</font>** | **<font style="color:rgb(0,0,0);">最大值</font>** | **<font style="color:rgb(0,0,0);">最小值</font>** | **<font style="color:rgb(0,0,0);">步进值</font>** | **小数部分位数** | **最大长度** | **<font style="color:rgb(0,0,0);">枚举取值列表</font>** | **<font style="color:rgb(0,0,0);">取值单位</font>** | **<font style="color:rgb(0,0,0);">特征说明</font>** |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| <font style="color:rgb(0,0,0);">illuminance</font> | <font style="color:rgb(0,0,0);">float</font> | <font style="color:rgb(0,0,0);">R</font> | <font style="color:rgb(0,0,0);"></font> | | | <font style="color:rgb(0,0,0);">2</font> | <font style="color:rgb(0,0,0);"></font> | | <font style="color:rgb(0,0,0);">Lux</font> | <font style="color:rgb(0,0,0);">光照强度值</font> |










A.5 PLC可编程控制器

| **服务类型标识** | **类型说明** | **特征列表** |
| --- | --- | --- |
| <font style="color:rgb(0,0,0);">ethInfo</font> | 以太网服务相关信息 | mac |
| | | ip |
| os | OS基本信息 | version |
| cpu | CPU基本信息 | manufacturer |
| | | model |
| | | currentUsage |
| | | usageThreshold |
| restart | 设备重启服务 | restart |
| <font style="color:rgb(0,0,0);">motherBoard</font> | <font style="color:rgb(0,0,0);">主板相关信息</font> | <font style="color:rgb(0,0,0);">v</font><font style="color:rgb(0,0,0);">ersion</font> |
| | | <font style="color:rgb(0,0,0);">model</font> |
| cust.basicStatus | PLC基础状态集 | cust.powerState |
| | | cust.runState |
| faultStatus | 异常状态服务<br/>(通过实例化既可描述PLC硬件故障也可描述软件故障） | code |
| | | <font style="color:rgb(0,0,0);">message</font> |
| Cust.analog | PLC模拟量服务 | <font style="color:rgb(0,0,0);">cust.</font><font style="color:rgb(0,0,0);">i</font><font style="color:rgb(0,0,0);">nputAddress</font> |
| | | <font style="color:rgb(0,0,0);">cust.</font><font style="color:rgb(0,0,0);">i</font><font style="color:rgb(0,0,0);">nputAnalogValue</font> |
| | | <font style="color:rgb(0,0,0);">cust.outputAddress</font> |
| | | <font style="color:rgb(0,0,0);">cust.outputAnalogValue</font> |
| Cust.digital | PLC数字量服务 | <font style="color:rgb(0,0,0);">cust.</font><font style="color:rgb(0,0,0);">i</font><font style="color:rgb(0,0,0);">nputAddress</font> |
| | | <font style="color:rgb(0,0,0);">cust.</font><font style="color:rgb(0,0,0);">i</font><font style="color:rgb(0,0,0);">nputDigitalValue</font> |
| | | <font style="color:rgb(0,0,0);">cust.outputAddress</font> |
| | | <font style="color:rgb(0,0,0);">cust.outputDigitalValue</font> |
| Cust.PLCTimer | PLC定时器服务 | <font style="color:rgb(0,0,0);">Cust.bindIOAddress</font> |
| | | <font style="color:rgb(0,0,0);">Cust.timerNumber</font> |
| | | <font style="color:rgb(0,0,0);">Cust.resetAddress</font> |
| | | <font style="color:rgb(0,0,0);">Cust.currentTime</font> |
| | | <font style="color:rgb(0,0,0);">Cust.presetTime</font> |
| Cust.PLCCounter | PLC计数器服务 | <font style="color:rgb(0,0,0);">Cust.bindIOAddress</font> |
| | | <font style="color:rgb(0,0,0);">Cust.counterNumber</font> |
| | | <font style="color:rgb(0,0,0);">Cust.resetAddress</font> |
| | | <font style="color:rgb(0,0,0);">Cust.currentValue</font> |
| | | <font style="color:rgb(0,0,0);">Cust.presetValue</font> |
| Cust.modbus | Modbus服务 | <font style="color:rgb(0,0,0);">Cust.modbus.type</font> |
| | | <font style="color:rgb(0,0,0);">Cust.modbus.port</font> |
| | | <font style="color:rgb(0,0,0);">ip</font> |
| | | <font style="color:rgb(0,0,0);">Cust.modbus.device</font> |
| | | <font style="color:rgb(0,0,0);">Cust.modbus.baud</font> |
| | | <font style="color:rgb(0,0,0);">Cust.modbus.parity</font> |
| | | <font style="color:rgb(0,0,0);">Cust.modbus.data_bits</font> |
| | | <font style="color:rgb(0,0,0);">Cust.modbus.stop_bits</font> |
| | | <font style="color:rgb(0,0,0);">Cust.modbus.slaveId</font> |
| | | <font style="color:rgb(0,0,0);">Cust.modbus.address</font> |
| | | <font style="color:rgb(0,0,0);">Cust.modbus.size</font> |
|  |  | <font style="color:rgb(0,0,0);"></font> |






| **<font style="color:rgb(0,0,0);">特征名称</font>** | **<font style="color:rgb(0,0,0);">特征数据类型</font>** | **<font style="color:rgb(0,0,0);">读写标识</font>** | **<font style="color:rgb(0,0,0);">最大值</font>** | **<font style="color:rgb(0,0,0);">最小值</font>** | **<font style="color:rgb(0,0,0);">步进值</font>** | **小数部分位数** | **最大长度** | **<font style="color:rgb(0,0,0);">枚举取值列表</font>** | **<font style="color:rgb(0,0,0);">取值单位</font>** | **<font style="color:rgb(0,0,0);">特征说明</font>** |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| <font style="color:rgb(0,0,0);">cust.powerState</font> | <font style="color:rgb(0,0,0);">Enum</font> | <font style="color:rgb(0,0,0);">R</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">0：供电缺失</font><br/><font style="color:rgb(0,0,0);">1：电源接通</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">P</font><font style="color:rgb(0,0,0);">LC当前供电状态</font> |
| <font style="color:rgb(0,0,0);">cust.runState</font> | <font style="color:rgb(0,0,0);">Enum</font> | <font style="color:rgb(0,0,0);">R</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">0：停止</font><br/><font style="color:rgb(0,0,0);">1：运行</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">P</font><font style="color:rgb(0,0,0);">LC当前运行状态</font> |
| <font style="color:rgb(0,0,0);">cust.inputAddress</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">模拟量</font><font style="color:rgb(0,0,0);">/数字量输入点位地址</font> |
| <font style="color:rgb(0,0,0);">cust.outputAddress</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">模拟量</font><font style="color:rgb(0,0,0);">/数字量输出点位地址</font> |
| <font style="color:rgb(0,0,0);">cust.inputAnalogValue</font> | <font style="color:rgb(0,0,0);">float</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">3</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">模拟量输入值（温度</font><font style="color:rgb(0,0,0);">/压力/湿度等）</font> |
| <font style="color:rgb(0,0,0);">cust.outputAnalogValue</font> | <font style="color:rgb(0,0,0);">f</font><font style="color:rgb(0,0,0);">loat</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">3</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">模拟量输出值（温度</font><font style="color:rgb(0,0,0);">/压力/湿度等）</font> |
| <font style="color:rgb(0,0,0);">cust.inputDigitalValue</font> | <font style="color:rgb(0,0,0);">Bool</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">数字量</font><font style="color:rgb(0,0,0);">(开关量)状态输入</font> |
| <font style="color:rgb(0,0,0);">cust.outputDigitalValue</font> | <font style="color:rgb(0,0,0);">Bool</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">数字量</font><font style="color:rgb(0,0,0);">(开关量)状态输出</font> |
| <font style="color:rgb(0,0,0);">Cust.bindIOAddress</font> | <font style="color:rgb(0,0,0);">String</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">6</font><font style="color:rgb(0,0,0);">4</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">与某个计数器</font><font style="color:rgb(0,0,0);">/计时器绑定的数字量/模拟量的IO地址对。该地址对将作为某计数器/计时器的输入输出口；</font><br/><font style="color:rgb(0,0,0);">字符串地址对的具体格式如下</font><font style="color:rgb(0,0,0);">：</font><br/><font style="color:rgb(0,0,0);">“</font>input1:output1<font style="color:rgb(0,0,0);">,</font><br/><font style="color:rgb(0,0,0);">input2:output2”</font> |
| <font style="color:rgb(0,0,0);">Cust.resetAddress</font> | <font style="color:rgb(0,0,0);">s</font><font style="color:rgb(0,0,0);">tring</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">3</font><font style="color:rgb(0,0,0);">2</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">与某个计数器</font><font style="color:rgb(0,0,0);">/计时器绑定的数字量/模拟量的</font><font style="color:rgb(0,0,0);">点位</font><font style="color:rgb(0,0,0);">地址。该地址可将某计数器</font><font style="color:rgb(0,0,0);">/计时器复位</font><br/><font style="color:rgb(0,0,0);">字符串的具体格式如下</font><font style="color:rgb(0,0,0);">：</font><font style="color:rgb(0,0,0);">“address</font><font style="color:rgb(0,0,0);">1</font><font style="color:rgb(0,0,0);">”或“address</font><font style="color:rgb(0,0,0);">1:address2</font><font style="color:rgb(0,0,0);">”</font> |
| <font style="color:rgb(0,0,0);">Cust.timerNumber</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">计时器的实例编号</font> |
| <font style="color:rgb(0,0,0);">Cust.currentTime</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">秒</font> | <font style="color:rgb(0,0,0);">计时器当前时间</font> |
| <font style="color:rgb(0,0,0);">Cust.presetTime</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">秒</font> | <font style="color:rgb(0,0,0);">计时器设定时间</font> |
| <font style="color:rgb(0,0,0);">Cust.counterNumber</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">计数器的实例编号</font> |
| <font style="color:rgb(0,0,0);">Cust.currentValue</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">次</font> | <font style="color:rgb(0,0,0);">计数器的当前数值</font> |
| <font style="color:rgb(0,0,0);">Cust.presetValue</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">次</font> | <font style="color:rgb(0,0,0);">计数器的</font><font style="color:rgb(0,0,0);">设定数值</font> |
| <font style="color:rgb(0,0,0);">Cust.modbus.type</font> | <font style="color:rgb(0,0,0);">e</font><font style="color:rgb(0,0,0);">num</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">0: TCP</font><br/><font style="color:rgb(0,0,0);">1: RTU</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">M</font><font style="color:rgb(0,0,0);">odbus工作模式</font> |
| <font style="color:rgb(0,0,0);">Cust.modbus.port</font> | <font style="color:rgb(0,0,0);">Uint16</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">TCP端口号</font> |
| <font style="color:rgb(0,0,0);">Cust.modbus.device</font> | <font style="color:rgb(0,0,0);">string</font> | <font style="color:rgb(0,0,0);">R</font><font style="color:rgb(0,0,0);">/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">RTU设备</font> |
| <font style="color:rgb(0,0,0);">Cust.modbus.baud</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">RTU波特率</font> |
| <font style="color:rgb(0,0,0);">Cust.modbus.parity</font> | <font style="color:rgb(0,0,0);">e</font><font style="color:rgb(0,0,0);">num</font> | <font style="color:rgb(0,0,0);">R/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">0: 无校验</font><br/><font style="color:rgb(0,0,0);">1: 奇数校验</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">RTU模式奇偶校验</font> |
| <font style="color:rgb(0,0,0);">Cust.modbus.data_bits</font> | <font style="color:rgb(0,0,0);">Uint8</font> | <font style="color:rgb(0,0,0);">R/W</font> | <font style="color:rgb(0,0,0);">8</font> | <font style="color:rgb(0,0,0);">5</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">RTU模式参数数据位</font> |
| <font style="color:rgb(0,0,0);">Cust.modbus.stop_bits</font> | <font style="color:rgb(0,0,0);">Uint8</font> | <font style="color:rgb(0,0,0);">R/W</font> | <font style="color:rgb(0,0,0);">2</font> | <font style="color:rgb(0,0,0);">1</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">RTU模式参数停止位</font> |
| <font style="color:rgb(0,0,0);">Cust.modbus.slaveId</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">从站号</font> |
| <font style="color:rgb(0,0,0);">Cust.modbus.address</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">数据地址</font> |
| <font style="color:rgb(0,0,0);">Cust.modbus.size</font> | <font style="color:rgb(0,0,0);">Uint32</font> | <font style="color:rgb(0,0,0);">R/W</font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);"></font> | <font style="color:rgb(0,0,0);">数据长度</font> |
